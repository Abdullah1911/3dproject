#include <opencv2/video/video.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "imageUndistortion.h"

ImageUndistortor::ImageUndistortor()
{
	m_distCoeffsMat = cv::Mat(5, 1, CV_64F);
	m_distCoeffsMat.at<double>(0, 0) = -4.9262466615852402e-001;
	m_distCoeffsMat.at<double>(1, 0) = 1.0883853572356499e+000;
	m_distCoeffsMat.at<double>(2, 0) = -3.9778469620185570e-003;
	m_distCoeffsMat.at<double>(3, 0) = -4.1125531523276207e-003;
	m_distCoeffsMat.at<double>(4, 0) = -2.0511374389081478e+000;

	m_cameraMatrixMat = cv::Mat(3, 3, CV_64F);
	m_cameraMatrixMat.at<double>(0, 0) = 2.1984690542080136e+002;
	m_cameraMatrixMat.at<double>(0, 1) = 0.;
	m_cameraMatrixMat.at<double>(0, 2) = 1.1422996755695458e+002;
	m_cameraMatrixMat.at<double>(1, 0) = 0.;
	m_cameraMatrixMat.at<double>(1, 1) = 2.1907427404038518e+002;
	m_cameraMatrixMat.at<double>(1, 2) = 1.3339885990322043e+002;
	m_cameraMatrixMat.at<double>(2, 0) = 0.;
	m_cameraMatrixMat.at<double>(2, 1) = 0.;
	m_cameraMatrixMat.at<double>(2, 2) = 1.;
}

ImageUndistortor::ImageUndistortor(const cv::Mat& img) : m_img(img.clone())
{
    m_distCoeffsMat = cv::Mat(5, 1, CV_64F);
	m_distCoeffsMat.at<double>(0, 0) = -4.9262466615852402e-001;
	m_distCoeffsMat.at<double>(1, 0) = 1.0883853572356499e+000;
	m_distCoeffsMat.at<double>(2, 0) = -3.9778469620185570e-003;
	m_distCoeffsMat.at<double>(3, 0) = -4.1125531523276207e-003;
	m_distCoeffsMat.at<double>(4, 0) = -2.0511374389081478e+000;

    m_cameraMatrixMat = cv::Mat(3, 3, CV_64F);
	m_cameraMatrixMat.at<double>(0, 0) = 2.1984690542080136e+002;
	m_cameraMatrixMat.at<double>(0, 1) = 0.;
	m_cameraMatrixMat.at<double>(0, 2) = 1.1422996755695458e+002;
	m_cameraMatrixMat.at<double>(1, 0) = 0.;
	m_cameraMatrixMat.at<double>(1, 1) = 2.1907427404038518e+002;
	m_cameraMatrixMat.at<double>(1, 2) = 1.3339885990322043e+002;
	m_cameraMatrixMat.at<double>(2, 0) = 0.;
	m_cameraMatrixMat.at<double>(2, 1) = 0.;
	m_cameraMatrixMat.at<double>(2, 2) = 1.;
}

void ImageUndistortor::setInitImg(const cv::Mat& img)
{
	m_img = img.clone();
}
cv::Mat ImageUndistortor::getInitImg() const
{
	return m_img;
}
cv::Mat ImageUndistortor::getUndistImg() const
{
	return m_undisImg;
}
void ImageUndistortor::imageUndistort()
{
	cv::undistort(m_img, m_undisImg, m_cameraMatrixMat, m_distCoeffsMat);

}

void ImageUndistortor::imageUndistortWithInput(const cv::Mat& inputImg, cv::Mat& resImg)
{
	cv::undistort(inputImg, resImg, m_cameraMatrixMat, m_distCoeffsMat, m_cameraMatrixMat);
}

ImageUndistortor::~ImageUndistortor()
{

}
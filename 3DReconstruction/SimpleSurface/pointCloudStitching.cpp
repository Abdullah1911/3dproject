#include "pointCloudStitching.h"
#include "surfacegraph.h"

#include <QtWidgets\QApplication>



pointCloudStitcher::pointCloudStitcher(const std::string& imagePath)
{
	cv::Mat initImg = cv::imread(imagePath);
	cv::Mat inputImgInit = initImg.clone();

	cv::Rect cropedRect = cv::Rect(10, 30, initImg.cols - 60, initImg.rows - 60);

	cv::Mat inputImg1 = initImg(cropedRect);

	m_currentImage = initImg.clone();// inputImg1.clone();
	initImg = initImg.clone();// inputImg1.clone();
	//	cv::imshow("Croped Image", inputImg);

	cv::cvtColor(initImg, initImg, CV_BGR2GRAY);
	cv::equalizeHist(initImg, initImg);

	cv::cvtColor(initImg, initImg, CV_GRAY2BGR);
	
	QImage heightMapImage((uchar*)initImg.data, initImg.cols, initImg.rows, initImg.step, QImage::Format_RGB888);


	std::cout << "Height map image size " << heightMapImage.width() << std::endl;
	m_heightMapProxy = new QHeightMapSurfaceDataProxy(heightMapImage);
	
	m_heightMapSeries = new QSurface3DSeries(m_heightMapProxy);
	m_heightMapSeries->setItemLabelFormat(QStringLiteral("(@xLabel, @zLabel): @yLabel"));

	std::cout << "Height Map series init " << m_heightMapSeries->dataProxy()->columnCount() << std::endl;



	m_heightMapProxy->setValueRanges(34.0f, 40.0f, 18.0f, 24.0f);



	m_heightMapWidth = heightMapImage.width();
	m_heightMapHeight = heightMapImage.height();

	pcl::PointCloud<pcl::PointXYZRGB> cloud;
	QApplication::processEvents();

	std::string pointCloudPath = "1.pcd";

	cloud = writeDataToPCDFile(m_heightMapSeries,pointCloudPath, inputImgInit);

}


void pointCloudStitcher::loadDataCloud(const PointCloud::Ptr tmpCloud, std::vector<pcdNameSpace::PCD, Eigen::aligned_allocator<pcdNameSpace::PCD> > &models)
{

	//check that the argument is a pcd file

	// Load the cloud and saves it into the global list of models
	pcdNameSpace::PCD m;
	
	//remove NAN points from the cloud
	std::vector<int> indices;
	m.cloud = tmpCloud;
	pcl::removeNaNFromPointCloud(*m.cloud, *m.cloud, indices);

	models.push_back(m);


}

void pointCloudStitcher::showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source)
{
	p->removePointCloud("vp1_target");
	p->removePointCloud("vp1_source");

	PointCloudColorHandlerCustom<PointT> tgt_h(cloud_target, 0, 255, 0);
	PointCloudColorHandlerCustom<PointT> src_h(cloud_source, 255, 0, 0);
	//  p->addPointCloud (cloud_target, /*tgt_h,*/ "vp1_target", vp_1);
	p->addPointCloud(cloud_source, /*src_h,*/ "vp1_source", vp_1);

	PCL_INFO("Press q to begin the registration.\n");
	p->spin();
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the second viewport of the visualizer
*
*/
void  pointCloudStitcher::showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source)
{
	p->removePointCloud("source");
	p->removePointCloud("target");


	PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler(cloud_target, "curvature");
	if (!tgt_color_handler.isCapable())
		PCL_WARN("Cannot create curvature color handler!");

	PointCloudColorHandlerGenericField<PointNormalT> src_color_handler(cloud_source, "curvature");
	if (!src_color_handler.isCapable())
		PCL_WARN("Cannot create curvature color handler!");


	p->addPointCloud(cloud_target, tgt_color_handler, "target", vp_2);
	p->addPointCloud(cloud_source, src_color_handler, "source", vp_2);


	p->spinOnce();

	p->addCoordinateSystem(1.0, "source", 0);

}



void pointCloudStitcher::pairAlign(const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr& output, Eigen::Matrix4f &final_transform, std::ofstream& myFile, bool downsample)
{
	downsample = false;
	//
	// Downsample for consistency and speed
	// \note enable this for large datasets
	PointCloud::Ptr src(new PointCloud);
	PointCloud::Ptr tgt(new PointCloud);
	pcl::VoxelGrid<PointT> grid;
	if (downsample)
	{
		grid.setLeafSize(0.05, 0.05, 0.05);
		grid.setInputCloud(cloud_src);
		grid.filter(*src);

		grid.setInputCloud(cloud_tgt);
		grid.filter(*tgt);
	}
	else
	{
		src = cloud_src;
		tgt = cloud_tgt;
	}


	// Compute surface normals and curvature
	PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
	PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

	pcl::NormalEstimation<PointT, PointNormalT> norm_est;
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
	norm_est.setSearchMethod(tree);
	norm_est.setKSearch(30);

	norm_est.setInputCloud(src);
	norm_est.compute(*points_with_normals_src);
	pcl::copyPointCloud(*src, *points_with_normals_src);

	norm_est.setInputCloud(tgt);
	norm_est.compute(*points_with_normals_tgt);
	pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

	//
	// Instantiate our custom point representation (defined above) ...
	MyPointRepresentation point_representation;
	// ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
	float alpha[4] = { 1.0, 1.0, 1.0, 1.0 };
	point_representation.setRescaleValues(alpha);

	//
	// Align
	pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;

	reg.setTransformationEpsilon(1e-6);
	// Set the maximum distance between two correspondences (src<->tgt) to 10cm
	// Note: adjust this based on the size of your datasets
	reg.setMaxCorrespondenceDistance(0.1);
	// Set the point representation
	reg.setPointRepresentation(boost::make_shared<const MyPointRepresentation>(point_representation));

	reg.setInputSource(points_with_normals_src);
	reg.setInputTarget(points_with_normals_tgt);



	//
	// Run the same optimization in a loop and visualize the results
	Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
	PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
	reg.setMaximumIterations(2);
	for (int i = 0; i < 20; ++i)
	{
		PCL_INFO("Iteration Nr. %d.\n", i);

		// save cloud for visualization purpose
		points_with_normals_src = reg_result;

		// Estimate
		reg.setInputSource(points_with_normals_src);
		reg.align(*reg_result);

		//accumulate transformation between each Iteration
		Ti = reg.getFinalTransformation() * Ti;
		Eigen::Matrix4f matrixOfRotTr = reg.getFinalTransformation();
		std::cout << "Matrix elements" << std::endl;
		//	  myFile<<matrixOfRotTr<<"\n";
		std::cout << matrixOfRotTr(0, 0) << "  " << matrixOfRotTr(0, 1) << "  " << matrixOfRotTr(0, 2) << "  " << matrixOfRotTr(0, 3) << std::endl;;
		std::cout << matrixOfRotTr(1, 0) << "  " << matrixOfRotTr(1, 1) << "  " << matrixOfRotTr(1, 2) << "  " << matrixOfRotTr(1, 3) << std::endl;;
		std::cout << matrixOfRotTr(2, 0) << "  " << matrixOfRotTr(2, 1) << "  " << matrixOfRotTr(2, 2) << "  " << matrixOfRotTr(2, 3) << std::endl;;
		std::cout << matrixOfRotTr(3, 0) << "  " << matrixOfRotTr(3, 1) << "  " << matrixOfRotTr(3, 2) << "  " << matrixOfRotTr(3, 3) << std::endl;;
		std::cout << " ROta and trans matrix size " << matrixOfRotTr.size();
		// std::cout<<"Rot matrix "<<
		//if the difference between this transformation and the previous one
		//is smaller than the threshold, refine the process by reducing
		//the maximal correspondence distance
		if (fabs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon())
			reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - 0.001);

		prev = reg.getLastIncrementalTransformation();

		// visualize current state
		showCloudsRight(points_with_normals_tgt, points_with_normals_src);
	}

	//
	// Get the transformation from target to source
	targetToSource = Ti.inverse();

	//
	// Transform target back in source frame
	targetToSource(0, 0) = 1;
	targetToSource(1, 1) = 1;
	targetToSource(3, 3) = 1;
	pcl::transformPointCloud(*cloud_tgt, *output, targetToSource);
	myFile << targetToSource << "\n";


	PointCloudColorHandlerCustom<PointT> cloud_tgt_h(output, 0, 255, 0);
	PointCloudColorHandlerCustom<PointT> cloud_src_h(cloud_src, 255, 0, 0);



	PCL_INFO("Press q to continue the registration.\n");


	//add the source to the transformed target

	*output += *cloud_src;


	final_transform = targetToSource;
}



pcl::PointCloud<pcl::PointXYZRGB> pointCloudStitcher::generatePointCloudFromImage(const std::string& imagePath,int indexOfCloud)
{
	cv::Mat inputImg = cv::imread(imagePath);
	cv::Mat inputImgInit = inputImg.clone();
	//cv::imshow("Original Image", inputImg);
	cv::Rect cropedRect = cv::Rect(10, 30, inputImg.cols - 60, inputImg.rows - 60);

	cv::Mat inputImg1 = inputImg(cropedRect);

  // inputImg = inputImg1.clone();
	

	cv::cvtColor(inputImg, inputImg, CV_BGR2GRAY);
	cv::equalizeHist(inputImg, inputImg);

	cv::cvtColor(inputImg, inputImg, CV_GRAY2BGR);

	QImage heightMapImage((uchar*)inputImg.data, inputImg.cols, inputImg.rows, inputImg.step, QImage::Format_RGB888);
	
	
//	m_heightMapProxy->setHeightMap(heightMapImage);
	QHeightMapSurfaceDataProxy* tmpheightMapProxy = new QHeightMapSurfaceDataProxy(heightMapImage);


	QSurface3DSeries *tmpheightMapSeries = new QSurface3DSeries(tmpheightMapProxy);

	tmpheightMapSeries->setItemLabelFormat(QStringLiteral("(@xLabel, @zLabel): @yLabel"));

	tmpheightMapProxy->setValueRanges(34.0f, 40.0f, 18.0f, 24.0f);

	QApplication::processEvents();
	
	/*QSurfaceDataArray *dataArray = new QSurfaceDataArray;
	dataArray->reserve(m_heightMapProxy->rowCount());
	for (int i = 0; i < m_heightMapProxy->rowCount(); i++) {
		QSurfaceDataRow *newRow = new QSurfaceDataRow(m_heightMapProxy->columnCount());
		
		
		for (int j = 0; j < m_heightMapProxy->columnCount(); j++) {
			
			(*newRow)[index++].setPosition(QVector3D(10, 10, 10));
		}
		*dataArray << newRow;
	}

	m_sqrtSinProxy->resetArray(dataArray);
	*/

	
   // m_heightMapSeries->dataProxy()->array()->at(0)->at(0)->setX(11);
	//m_heightMapSeries = new QSurface3DSeries(m_heightMapProxy);

//	m_heightMapSeries = new Q;
   // m_heightMapSeries->setDataProxy(m_heightMapProxy);
	
	
	m_heightMapWidth = heightMapImage.width();
	m_heightMapHeight = heightMapImage.height();
	//cv::imshow("INT IMG", inputImg);
//	cv::waitKey(0);
	


	//m_heightMapSeries->setItemLabelFormat(QStringLiteral("(@xLabel, @zLabel): @yLabel"));
	pcl::PointCloud<pcl::PointXYZRGB> cloud;

	std::string pointCloudPath =  "pointCloudSet\\" + std::to_string(unsigned long(indexOfCloud)) +  ".pcd";

	cloud = writeDataToPCDFile(tmpheightMapSeries,pointCloudPath, inputImgInit);
//	delete tmp3dSeries;
	return cloud;

	


}








pcl::PointCloud<pcl::PointXYZRGB> pointCloudStitcher::writeDataToPCDFile(QSurface3DSeries * heightMapSeries, const std::string& filePath, const cv::Mat& inputMat)
{
	std::cout << "Height map serises width " << heightMapSeries->dataProxy()->columnCount() << std::endl;
	cv::Mat inputImg1 = inputMat.clone();
	cv::Rect cropedRect = cv::Rect(10, 30, inputImg1.cols - 60, inputImg1.rows - 60);
	cv::Mat inputImg = inputImg1.clone();// inputImg1(cropedRect);
	
//	cv::Mat depthMapImage = cv::imread("depthMap.png");
	cv::Mat inputImgRot = inputImg.clone();



	double angle = -90;  // or 270
	cv::Size src_sz = inputImgRot.size();
	cv::Size dst_sz(src_sz.height, src_sz.width);
	
	int len = std::max(inputImgRot.cols, inputImgRot.rows);
	cv::Point2f center(len / 2., len / 2.);
	cv::Mat rot_mat = cv::getRotationMatrix2D(center, angle, 1.0);
	cv::warpAffine(inputImgRot, inputImgRot, rot_mat, dst_sz);
	//cv::warpAffine(depthMapImage, depthMapImage, rot_mat, dst_sz);
	
	cv::Mat imgRotTr = inputImgRot.clone();
	for (int i = 0; i < imgRotTr.rows / 2; ++i) {
		for (int j = 0; j < imgRotTr.cols; ++j) {
			imgRotTr.at<cv::Vec3b>(i, j) = inputImg.at<cv::Vec3b>(imgRotTr.rows - 1 - i, j);
			imgRotTr.at<cv::Vec3b>(imgRotTr.rows - 1 - i, j) = inputImg.at<cv::Vec3b>(i, j);
		}
	}

	pcl::PointCloud<pcl::PointXYZRGB> cloud;
//	cv::imwrite("rotImg.png", imgRotTr);

	std::string matrixPath = "QArt.xml";
	cv::FileStorage fs(matrixPath, cv::FileStorage::READ);


	cv::Mat Q;
	fs["Q"] >> Q;
	

	
	if (Q.cols != 4 || Q.rows != 4)
	{
		std::cerr << "ERROR: Could not read matrix Q (doesn't exist or size is not 4x4)" << std::endl;
		return cloud;
	}
	double Q03, Q13, Q23, Q32, Q33;
	double px, py, pz;
	Q03 = Q.at<double>(0, 3);
	Q13 = Q.at<double>(1, 3);
	Q23 = Q.at<double>(2, 3);
	Q32 = Q.at<double>(3, 2);
	Q33 = Q.at<double>(3, 3);
	



	cloud.width = heightMapSeries->dataProxy()->columnCount() * heightMapSeries->dataProxy()->rowCount();
	std::cout << "  point cloud width " << cloud.width << std::endl;
	cloud.height = 1;
	cloud.is_dense = false;
	cloud.points.resize(cloud.width * cloud.height);
	for (int i = 0; i < heightMapSeries->dataProxy()->rowCount(); ++i) {
		QSurfaceDataRow* tmpRow = heightMapSeries->dataProxy()->array()->at(i);
		for (int j = 0; j < tmpRow->size(); ++j) {
			uchar d = tmpRow->at(j).y();
		//	if (d == 0) continue; //Discard bad pixels 
			double pw = -1.0 * static_cast<double>(d)* Q32 + Q33;
			px = static_cast<double>(j)+Q03;
			py = static_cast<double>(i)+Q13;
			pz = Q23;
        
			px = px / pw;
			py = py / pw;
			pz = pz / pw;


			cloud.points[i*m_heightMapWidth + j].x =   px;//j;
			cloud.points[i*m_heightMapWidth + j].y = pz;// tmpRow->at(j).y(); 
			cloud.points[i* m_heightMapWidth + j].z = py;// i;
			cloud.points[i*m_heightMapWidth + j].b = imgRotTr.at<cv::Vec3b>(i, j)[0];
			cloud.points[i*m_heightMapWidth + j] .g = imgRotTr.at<cv::Vec3b>(i, j)[1];
			cloud.points[i*m_heightMapWidth + j].r = imgRotTr.at<cv::Vec3b>(i, j)[2];
			float tmpPixelVal = tmpRow->at(j).y();

		}

	}

	std::cout << "Point cloud data" << cloud.points.size() << std::endl;
//	pcl::io::savePCDFileASCII("test.pcd", cloud);

//	cv::imshow("Image 222", inputImg1);
//	cv::waitKey(0);
	pcl::io::savePCDFileASCII(filePath, cloud);
	
	std::cerr << "Saved " << cloud.points.size() << " data points to" << filePath << "  file " << std::endl;
	return cloud;
}
void pointCloudStitcher::readAndShowPointCloud(const std::string& pointCloudFilePath1, std::vector<std::string> pointCloudFilePath2)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud1(new pcl::PointCloud<pcl::PointXYZRGB>);
	
	if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pointCloudFilePath1, *cloud1) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file  .pcd \n");
		return;
	}
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud2(new pcl::PointCloud<pcl::PointXYZRGB>);
/*	if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pointCloudFilePath1, *cloud2) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file  .pcd \n");
		return;
	}
	*/
	
	for (int i = 0; i < pointCloudFilePath2.size(); ++i) {
		if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pointCloudFilePath2.at(i), *cloud2) == -1) //* load the file
		{
			PCL_ERROR("Couldn't read file  .pcd \n");
			return;
		}
		std::cout << "Loaded "
			<< cloud1->width * cloud1->height
			<< " data points from test_pcd.pcd with the following fields: "
			<< std::endl;

		*cloud1 = *cloud2 + *cloud1;
	}
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloud_in_color_h3(cloud1, (int)0, (int)0,

		(int)255);

	pcl::visualization::PCLVisualizer viewer("ICP DEMO");
	int v1(0);
	int v2(0);
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.createViewPort(0.5, 0, 1.0, 1.0, v2);
	viewer.addPointCloud(cloud2,/* cloud_in_color_h3,*/ "cloud in color", v1);
	viewer.addPointCloud(cloud1,/* cloud_in_color_h3,*/ "cloud in color1", v2);
	while (!viewer.wasStopped())
	{
		viewer.spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}

}

PointCloud::Ptr pointCloudStitcher::stitchPointCloudSet(std::vector < pcl::PointCloud<pcl::PointXYZRGB>> pointCloudVec)
{

	pcl::PointCloud<pcl::PointXYZRGB> resPointCloud;
	std::vector<pcdNameSpace::PCD, Eigen::aligned_allocator<pcdNameSpace::PCD> > data;
	int y1 = 0;
	for (int i = 0; i < pointCloudVec.size(); i++) {
	//	std::string depthMapPath = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\depthMapFolder\\resDepthImg" + std::to_string(long double(i)) + ".png";
		// std::string imagePath = "C:\\Users\\Artashes\\Desktop\\Stereo Vision Project\\StereoVision-ADCensus-master\\ImageRectify\\buildNew\\Release\\rectified\\image" + std::to_string(long double(i)) + "_1.png";
	//	std::string imagePath = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage" + std::to_string(long double(i)) + ".png";
	//	cv::Mat depthImg = cv::imread(depthMapImagePathVec.at(i));
	//	cv::Mat img = cv::imread(imagePath);
		//		cv::imshow("Depth image", depthImg);
		//		cv::imshow("Image ", img);
		//		cv::waitKey(0);
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_ptrTmp(&pointCloudVec.at(i));
		//generatePointCloudFromImage(leftImagePathVec.at(i), depthMapImagePathVec.at(i), QmatrixPath, point_cloud_ptrTmp);

		//point_cloud_ptrTmp->points.push_back(pointCloudVec.at(i);




		loadDataCloud(point_cloud_ptrTmp, data);
//		std::string pointCloudPathTmp = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\Release\\equlizeHistCloud\\" + std::to_string(long double(y1)) + ".pcd";

//		pcl::io::savePCDFile(pointCloudPathTmp, *point_cloud_ptrTmp, true);
		++y1;
	}
	PointCloud::Ptr result(new PointCloud), source, target;
	if (data.empty())
	{
		std::cout << "Point Cloud data set is empty" << std::endl;
	   //	PCL_ERROR("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
		PCL_ERROR("[*] - multiple files can be added. The registration results of (i, i+1) will be registered against (i+2), etc");
		return result;
	}
	PCL_INFO("Loaded %d datasets.", (int)data.size());

	
	Eigen::Matrix4f GlobalTransform = Eigen::Matrix4f::Identity(), pairTransform;
	std::cout << "Show Stitching res" << std::endl;
	for (size_t i = 1; i < data.size(); ++i)
	{
		std::ofstream myfile;
		std::string textFilePath = "RotAndTrour" + std::to_string(long double(i)) + ".txt";
		myfile.open(textFilePath);
		myfile << "Writing this to a file.\n";
		source = data[i - 1].cloud;
		target = data[i].cloud;

		// Add visualization data
		showCloudsLeft(source, target);


		PointCloud::Ptr temp(new PointCloud);
		PCL_INFO("Aligning %s (%d) with %s (%d).\n", data[i - 1].f_name.c_str(), source->points.size(), data[i].f_name.c_str(), target->points.size());
		pairAlign(source, target, temp, pairTransform, myfile, true);
		//myfile<<"\n";
		//myfile<<"\n";
		myfile.close();
		//transform current pair into the global transform
		pcl::transformPointCloud(*temp, *result, GlobalTransform);

		//update the global transform
		GlobalTransform = GlobalTransform * pairTransform;

		//save aligned pair, transformed into the first cloud's frame
		std::stringstream ss;
		ss << i << "pointCloudSet\\result777.pcd";
		pcl::io::savePCDFileASCII(ss.str(), *result);
		//	pcl::io::savePLYFile (ss.str (), *result, true);
	}






	return result;
}

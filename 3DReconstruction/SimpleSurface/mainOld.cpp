//#include <QtDataVisualization>

#include <QtDataVisualization/QtDataVisualization>
#include <QtDataVisualization/Q3DSurface>
#include <QtWidgets/QWidget>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLayout>




using namespace QtDataVisualization;

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Q3DSurface *graph = new Q3DSurface();
	QWidget *container = QWidget::createWindowContainer(graph);

	QWidget *widget = new QWidget;
	QHBoxLayout *hLayout = new QHBoxLayout(widget);
	QVBoxLayout *vLayout = new QVBoxLayout();
	hLayout->addWidget(container, 1);
	hLayout->addLayout(vLayout);
	vLayout->setAlignment(Qt::AlignTop);


	m_sqrtSinProxy = new QSurfaceDataProxy();
	m_sqrtSinSeries = new QSurface3DSeries(m_sqrtSinProxy);
	return 0;
}


/*
int main(int argc, char **argv)
{
	QGuiApplication app(argc, argv);

	Q3DSurface surface;
	surface.setFlags(surface.flags() ^ Qt::FramelessWindowHint);
	QSurfaceDataArray *data = new QSurfaceDataArray;
	QSurfaceDataRow *dataRow1 = new QSurfaceDataRow;
	QSurfaceDataRow *dataRow2 = new QSurfaceDataRow;

	*dataRow1 << QVector3D(0.0f, 0.1f, 0.5f) << QVector3D(1.0f, 0.5f, 0.5f);
	*dataRow2 << QVector3D(0.0f, 1.8f, 1.0f) << QVector3D(1.0f, 1.2f, 1.0f);
	*data << dataRow1 << dataRow2;

	QSurface3DSeries *series = new QSurface3DSeries;
	series->dataProxy()->resetArray(data);
	surface.addSeries(series);
	surface.show();

	return app.exec();
}
*/

#include <opencv2/core/core.hpp>
#include <vector>

typedef std::pair<std::vector<cv::Point2f>, std::vector<cv::Point2f>> feauturePointsPairs;
typedef std::vector<std::pair<cv::Point2f, cv::Point2f>> correspondPoints;

void LocalTemplateMatching(const cv::Mat& sourceImg1, const cv::Mat& sourceImg2, const cv::Size& rectSize, correspondPoints& correspondingPointsVec, std::vector<cv::Point2f>& pointsOpposite);


void MatchingMethod(const cv::Mat& sourceImg, const cv::Mat& tempImg, cv::Rect& resultRect);

feauturePointsPairs getFeauturePoints(const cv::Mat& img1, const cv::Mat& img2);
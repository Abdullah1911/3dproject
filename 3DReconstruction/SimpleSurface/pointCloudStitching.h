
#ifndef POINTCLOUDSTITCHING_H
#define POINTCLOUDSTITCHING_H


#include "surfacegraph.h"

#include <string>
#include <vector>
#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/voxel_grid.h>


#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>


#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>


using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

//pcl::visualization::PCLVisualizer *p;
//its left and right viewports
//int vp_1, vp_2;

//convenient structure to handle our pointclouds
namespace pcdNameSpace {
	struct PCD
	{
		PointCloud::Ptr cloud;
		std::string f_name;

		PCD() : cloud(new PointCloud) {};
	};

	struct PCDComparator
	{
		bool operator () (const PCD& p1, const PCD& p2)
		{
			return (p1.f_name < p2.f_name);
		}
	};
}

class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
	using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
	MyPointRepresentation()
	{
		// Define the number of dimensions
		nr_dimensions_ = 4;
	}

	// Override the copyToFloatArray method to define our feature vector
	virtual void copyToFloatArray(const PointNormalT &p, float * out) const
	{
		// < x, y, z, curvature >
		out[0] = p.x;
		out[1] = p.y;
		out[2] = p.z;
		out[3] = p.curvature;
	}
};

class pointCloudStitcher 
{
private:
	void loadDataCloud(const PointCloud::Ptr tmpCloud, std::vector<pcdNameSpace::PCD, Eigen::aligned_allocator<pcdNameSpace::PCD> > &models);
	void pairAlign(const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr& output, Eigen::Matrix4f &final_transform, std::ofstream& myFile, bool downsample);
private:
	
	QHeightMapSurfaceDataProxy *m_heightMapProxy;

	QSurface3DSeries *m_heightMapSeries;
	
	pcl::visualization::PCLVisualizer *p;
	int vp_1, vp_2;
	cv::Mat m_currentImage;

	int m_heightMapWidth;
	int m_heightMapHeight;

public:
	pointCloudStitcher(const std::string& imagePath);
	void showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source);
	void showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source);
	pcl::PointCloud<pcl::PointXYZRGB> writeDataToPCDFile(QSurface3DSeries * heightMapSeries, const std::string& filePath, const cv::Mat& inputMat);
	void  readAndShowPointCloud(const std::string& pointCloudFilePath1, std::vector<std::string>);
	pcl::PointCloud<pcl::PointXYZRGB> generatePointCloudFromImage(const std::string& imagePath,int indexOfPointCloud);
	PointCloud::Ptr stitchPointCloudSet(std::vector < pcl::PointCloud<pcl::PointXYZRGB>> pointCloudVec);
	~pointCloudStitcher();
	//void generatePointCloudSet(const std::string& imageDirPath);
	//void stitchPointCloudSet(const std::vector < pcl::PointCloud<pcl::PointXYZRGB>>& pointCloudVec, pcl::PointCloud < pcl::PointXYZRGB>& resPointCloud);
};

#endif
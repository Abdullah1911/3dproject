
#include <opencv2/core/core.hpp>


class ImageUndistortor{
public:
	ImageUndistortor();
	ImageUndistortor(const cv::Mat& img);
	void setInitImg(const cv::Mat& img);
	cv::Mat getInitImg() const;
	cv::Mat getUndistImg() const;
	void imageUndistort();
	void imageUndistortWithInput(const cv::Mat& inputImg, cv::Mat& resImg);
	~ImageUndistortor();
private:
	cv::Mat m_distCoeffsMat;
	cv::Mat m_cameraMatrixMat;
	cv::Mat m_img;
	cv::Mat m_undisImg;
};
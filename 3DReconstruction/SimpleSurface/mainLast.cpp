
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd
** All rights reserved.
** For any questions to The Qt Company, please use contact form at http://qt.io
**
** This file is part of the Qt Data Visualization module.
**
** Licensees holding valid commercial license for Qt may use this file in
** accordance with the Qt License Agreement provided with the Software
** or, alternatively, in accordance with the terms contained in a written
** agreement between you and The Qt Company.
**
** If you have questions regarding the use of this file, please use
** contact form at http://qt.io
**
****************************************************************************/

//#include <boost-1_57/boost/make_shared.hpp>

#include <fstream>

#include <boost/make_shared.hpp>


#include <pcl/filters/voxel_grid.h>


#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>


#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>



#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "surfacegraph.h"
#include "pointCloudStitching.h"


#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>




using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

// This is a tutorial so we can afford having global variables 
//our visualizer
//pcl::visualization::PCLVisualizer *p;
//its left and right viewports
//int vp_1, vp_2;

//convenient structure to handle our pointclouds







struct PCD
{
	PointCloud::Ptr cloud;
	std::string f_name;

	PCD() : cloud(new PointCloud) {};
};

struct PCDComparator
{
	bool operator () (const PCD& p1, const PCD& p2)
	{
		return (p1.f_name < p2.f_name);
	}
};



#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtGui/QPainter>
#include <QtGui/QScreen>


/*void writeDataToPCDFile(const QSurface3DSeries * heightMapSeries, const std::string& filePath)
{


}*/

/*
int main(int argc, char** argv)
{
	pcl::PointCloud<pcl::PointXYZ> cloud;

	// Fill in the cloud data
	cloud.width = 5;
	cloud.height = 1;
	cloud.is_dense = false;
	cloud.points.resize(cloud.width * cloud.height);

	for (size_t i = 0; i < cloud.points.size(); ++i)
	{
		cloud.points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
		cloud.points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
		cloud.points[i].z = 1024 * rand() / (RAND_MAX + 1.0f);
	}

	pcl::io::savePCDFileASCII("test_pcd.pcd", cloud);
	std::cerr << "Saved " << cloud.points.size() << " data points to test_pcd.pcd." << std::endl;

	for (size_t i = 0; i < cloud.points.size(); ++i)
		std::cerr << "    " << cloud.points[i].x << " " << cloud.points[i].y << " " << cloud.points[i].z << std::endl;

	return (0);
}*/




int main(int argc, char **argv)
{
	
	
	


	QApplication app(argc, argv);
/*	pointCloudStitcher* pointClSt = new pointCloudStitcher("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage150.png");
	pcl::PointCloud<pcl::PointXYZRGB> tmpPointCloud = pointClSt->generatePointCloudFromImage("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage1.png", 1);
	//pcl::PointCloud<pcl::PointXYZRGB> tmpPointCloud = pointClSt->generatePointCloudFromImage("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage1.png", 1);
	std::vector<std::string> pointCloudPathVec;
	pointClSt->readAndShowPointCloud("C:\\Users\\Artashes\\Documents\\Visual Studio 2013\\Projects\\SimpleSurface\\SimpleSurface\\pointCloudSet\\1.pcd", pointCloudPathVec);
	return 0;
	for (int i = 1; i < 24; ++i){
		std::string tmpPath = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\" + std::to_string(long long(i)) + "equlizeHist.pcd";
		pointCloudPathVec.push_back(tmpPath);
		

	}
//	pointClSt->generatePointCloudFromImage("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage1.png",);
	//pointClSt->readAndShowPointCloud("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\1equlizeHist.pcd",pointCloudPathVec);
	//pointClSt->readAndShowPointCloud("pointCloudSet\\0.pcd", pointCloudPathVec);

	std::vector<pcl::PointCloud<pcl::PointXYZRGB>> vectorOfPointClouds;
	for (int i = 0; i < 400; i+=5) {
		std::string tmpImgPath = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage" + std::to_string(unsigned long(i)) + ".png";
		pcl::PointCloud<pcl::PointXYZRGB> tmpPointCloud = pointClSt->generatePointCloudFromImage(tmpImgPath,i);
		vectorOfPointClouds.push_back(tmpPointCloud);
	}
	pointClSt->readAndShowPointCloud("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\1equlizeHist.pcd", pointCloudPathVec);
	return 0;
	PointCloud::Ptr pointCloudStitchRes;
	pointCloudStitchRes = pointClSt->stitchPointCloudSet(vectorOfPointClouds);
	return 0;*/
//	QApplication app(argc, argv);
	Q3DSurface *graph = new Q3DSurface();

	

	QWidget *container = QWidget::createWindowContainer(graph);

	QSize screenSize = graph->screen()->size();
	container->setMinimumSize(QSize(screenSize.width() / 2, screenSize.height() / 1.6));
	container->setMaximumSize(screenSize);
	container->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	container->setFocusPolicy(Qt::StrongFocus);

	QWidget *widget = new QWidget;
	QHBoxLayout *hLayout = new QHBoxLayout(widget);
	QVBoxLayout *vLayout = new QVBoxLayout();
	hLayout->addWidget(container, 1);
	hLayout->addLayout(vLayout);
	vLayout->setAlignment(Qt::AlignTop);

	widget->setWindowTitle(QStringLiteral("Surface example"));

	QGroupBox *modelGroupBox = new QGroupBox(QStringLiteral("Model"));

	QRadioButton *sqrtSinModelRB = new QRadioButton(widget);
	sqrtSinModelRB->setText(QStringLiteral("Sqrt && Sin"));
	sqrtSinModelRB->setChecked(false);

	QRadioButton *heightMapModelRB = new QRadioButton(widget);
	heightMapModelRB->setText(QStringLiteral("Height Map"));
	heightMapModelRB->setChecked(false);

	QVBoxLayout *modelVBox = new QVBoxLayout;
	modelVBox->addWidget(sqrtSinModelRB);
	modelVBox->addWidget(heightMapModelRB);
	modelGroupBox->setLayout(modelVBox);

	QGroupBox *selectionGroupBox = new QGroupBox(QStringLiteral("Selection Mode"));

	QRadioButton *modeNoneRB = new QRadioButton(widget);
	modeNoneRB->setText(QStringLiteral("No selection"));
	modeNoneRB->setChecked(false);

	QRadioButton *modeItemRB = new QRadioButton(widget);
	modeItemRB->setText(QStringLiteral("Item"));
	modeItemRB->setChecked(false);

	QRadioButton *modeSliceRowRB = new QRadioButton(widget);
	modeSliceRowRB->setText(QStringLiteral("Row Slice"));
	modeSliceRowRB->setChecked(false);

	QRadioButton *modeSliceColumnRB = new QRadioButton(widget);
	modeSliceColumnRB->setText(QStringLiteral("Column Slice"));
	modeSliceColumnRB->setChecked(false);

	QVBoxLayout *selectionVBox = new QVBoxLayout;
	selectionVBox->addWidget(modeNoneRB);
	selectionVBox->addWidget(modeItemRB);
	selectionVBox->addWidget(modeSliceRowRB);
	selectionVBox->addWidget(modeSliceColumnRB);
	selectionGroupBox->setLayout(selectionVBox);

	QSlider *axisMinSliderX = new QSlider(Qt::Horizontal, widget);
	axisMinSliderX->setMinimum(0);
	axisMinSliderX->setTickInterval(1);
	axisMinSliderX->setEnabled(true);
	QSlider *axisMaxSliderX = new QSlider(Qt::Horizontal, widget);
	axisMaxSliderX->setMinimum(1);
	axisMaxSliderX->setTickInterval(1);
	axisMaxSliderX->setEnabled(true);
	QSlider *axisMinSliderZ = new QSlider(Qt::Horizontal, widget);
	axisMinSliderZ->setMinimum(0);
	axisMinSliderZ->setTickInterval(1);
	axisMinSliderZ->setEnabled(true);
	QSlider *axisMaxSliderZ = new QSlider(Qt::Horizontal, widget);
	axisMaxSliderZ->setMinimum(1);
	axisMaxSliderZ->setTickInterval(1);
	axisMaxSliderZ->setEnabled(true);

	QComboBox *themeList = new QComboBox(widget);
	themeList->addItem(QStringLiteral("Qt"));
	themeList->addItem(QStringLiteral("Primary Colors"));
	themeList->addItem(QStringLiteral("Digia"));
	themeList->addItem(QStringLiteral("Stone Moss"));
	themeList->addItem(QStringLiteral("Army Blue"));
	themeList->addItem(QStringLiteral("Retro"));
	themeList->addItem(QStringLiteral("Ebony"));
	themeList->addItem(QStringLiteral("Isabelle"));

	QGroupBox *colorGroupBox = new QGroupBox(QStringLiteral("Custom gradient"));

	QLinearGradient grBtoY(0, 0, 1, 100);
	grBtoY.setColorAt(1.0, Qt::black);
	grBtoY.setColorAt(0.67, Qt::blue);
	grBtoY.setColorAt(0.33, Qt::red);
	grBtoY.setColorAt(0.0, Qt::yellow);
	QPixmap pm(24, 100);
	QPainter pmp(&pm);
	pmp.setBrush(QBrush(grBtoY));
	pmp.setPen(Qt::NoPen);
	pmp.drawRect(0, 0, 24, 100);
	QPushButton *gradientBtoYPB = new QPushButton(widget);
	gradientBtoYPB->setIcon(QIcon(pm));
	gradientBtoYPB->setIconSize(QSize(24, 100));

	QLinearGradient grGtoR(0, 0, 1, 100);
	grGtoR.setColorAt(1.0, Qt::darkGreen);
	grGtoR.setColorAt(0.5, Qt::yellow);
	grGtoR.setColorAt(0.2, Qt::red);
	grGtoR.setColorAt(0.0, Qt::darkRed);
	pmp.setBrush(QBrush(grGtoR));
	pmp.drawRect(0, 0, 24, 100);
	QPushButton *gradientGtoRPB = new QPushButton(widget);
	gradientGtoRPB->setIcon(QIcon(pm));
	gradientGtoRPB->setIconSize(QSize(24, 100));

	QHBoxLayout *colorHBox = new QHBoxLayout;
	colorHBox->addWidget(gradientBtoYPB);
	colorHBox->addWidget(gradientGtoRPB);
	colorGroupBox->setLayout(colorHBox);

	vLayout->addWidget(modelGroupBox);
	vLayout->addWidget(selectionGroupBox);
	vLayout->addWidget(new QLabel(QStringLiteral("Column range")));
	vLayout->addWidget(axisMinSliderX);
	vLayout->addWidget(axisMaxSliderX);
	vLayout->addWidget(new QLabel(QStringLiteral("Row range")));
	vLayout->addWidget(axisMinSliderZ);
	vLayout->addWidget(axisMaxSliderZ);
	vLayout->addWidget(new QLabel(QStringLiteral("Theme")));
	vLayout->addWidget(themeList);
	vLayout->addWidget(colorGroupBox);

	widget->show();

	SurfaceGraph *modifier = new SurfaceGraph(graph);

//	modifier->readAndShowPointCloud("newPcdTest.pcd");
//	return 0;
	QObject::connect(heightMapModelRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::enableHeightMapModel);
	QObject::connect(sqrtSinModelRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::enableSqrtSinModel);
	QObject::connect(modeNoneRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::toggleModeNone);
	QObject::connect(modeItemRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::toggleModeItem);
	QObject::connect(modeSliceRowRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::toggleModeSliceRow);
	QObject::connect(modeSliceColumnRB, &QRadioButton::toggled,
		modifier, &SurfaceGraph::toggleModeSliceColumn);
	QObject::connect(axisMinSliderX, &QSlider::valueChanged,
		modifier, &SurfaceGraph::adjustXMin);
	QObject::connect(axisMaxSliderX, &QSlider::valueChanged,
		modifier, &SurfaceGraph::adjustXMax);
	QObject::connect(axisMinSliderZ, &QSlider::valueChanged,
		modifier, &SurfaceGraph::adjustZMin);
	QObject::connect(axisMaxSliderZ, &QSlider::valueChanged,
		modifier, &SurfaceGraph::adjustZMax);
	QObject::connect(themeList, SIGNAL(currentIndexChanged(int)),
		modifier, SLOT(changeTheme(int)));
	QObject::connect(gradientBtoYPB, &QPushButton::pressed,
		modifier, &SurfaceGraph::setBlackToYellowGradient);
	QObject::connect(gradientGtoRPB, &QPushButton::pressed,
		modifier, &SurfaceGraph::setGreenToRedGradient);

	modifier->setAxisMinSliderX(axisMinSliderX);
	modifier->setAxisMaxSliderX(axisMaxSliderX);
	modifier->setAxisMinSliderZ(axisMinSliderZ);
	modifier->setAxisMaxSliderZ(axisMaxSliderZ);

	sqrtSinModelRB->setChecked(true);
	modeItemRB->setChecked(true);
	themeList->setCurrentIndex(2);

	return app.exec();
}


/*int main()
{
	return 0;
}*/
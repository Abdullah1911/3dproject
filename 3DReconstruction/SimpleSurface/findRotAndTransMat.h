#include <opencv2/core/core.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>


#include <pcl/visualization/pcl_visualizer.h>

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;


typedef std::array<float, 3> float3;
typedef std::array<float3, 3> float3x3;

const float PI = 3.14159265358979323846264f;
typedef std::pair<std::vector<cv::Point2f>, std::vector<cv::Point2f>> feauturePointsPairs;


void findRotAndTransMatrixes(const std::string& img1, const std::string& img2, Eigen::Matrix4f& resultMat);
void findRotAndTransMatrixesWithTemplateMatch(const cv::Mat& img1, const cv::Mat& img2, const cv::Size& rectSize, Eigen::Matrix4f& resultMat, std::vector<double>& frameRTInfo);
void calculateAnglesFromRotMatrix(const cv::Mat& rotMatrix, double& X, double& Y, double& Z);
bool closeEnough(const float& a, const float& b, const float& epsilon1 = std::numeric_limits<float>::epsilon());
float3 eulerAngles(const float3x3& R);

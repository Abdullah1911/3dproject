#include "findRotAndTransMat.h"
#include "templateMatching.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/stitching/stitcher.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/legacy/legacy.hpp>

#include  <iostream>
#include <fstream>
#include <cmath>
#include <array>
#include <limits>


# define M_PI           3.14159265358979323846  /* pi */

void findRotAndTransMatrixes(const std::string& img1Path, const std::string& img2Path, Eigen::Matrix4f& resultMat)
{

	cv::Mat K(3, 3, CV_64F);
	/*K.at<double>(0, 0) = 1.7609925389059541e+002;
	K.at<double>(0, 2) = 1.2450000000000000e+002;
	K.at<double>(1, 0) = 0.;
	K.at<double>(1, 1) = 1.7550836356521134e+002;
	K.at<double>(1, 2) = 1.2450000000000000e+002;
	K.at<double>(2, 0) = 0.;
	K.at<double>(2, 1) = 0.;
	K.at<double>(2, 2) = 1;*/
	
    K.at<double>(0, 0) = 237.46;
	K.at<double>(0, 1) = 0;
	K.at<double>(0, 2) = 0;
	K.at<double>(1, 0) = 0;
	K.at<double>(1, 1) = 237.15;
	K.at<double>(1, 2) = 0;
	K.at<double>(2, 0) = 160.35;
	K.at<double>(2, 1) = 137.6620;
	K.at<double>(2, 2) = 1;
	
	
	/*cv::Mat distortionMat = cv::Mat(1, 5, CV_64F);
	distortionMat.at<double>(0, 0) = -0.399185080008440;
	distortionMat.at<double>(0, 1) = 0.133602838602991;
	distortionMat.at<double>(0, 2) = 0.0001;
	distortionMat.at<double>(0, 3) = 0.0001;
	distortionMat.at<double>(0, 4) = 0.0001; 
	*/





	cv::Mat img1 = cv::imread(img1Path);
	cv::Mat img2 = cv::imread(img2Path);
	feauturePointsPairs feauturePointsRes;
	cv::Mat img1Dist;
	cv::Mat img2Dist;
	

	cv::Rect cropedRect = cv::Rect(10, 30, img1.cols - 60, img2.rows - 60);

	//img1  = img1.clone()(cropedRect);
//	img2 = img2.clone()(cropedRect);

   
	cv::DenseFeatureDetector detector;
	cv::SiftDescriptorExtractor extractor;

	std::vector<cv::KeyPoint> keyPoints1, keyPoints2;
	detector.detect(img1, keyPoints1);
	detector.detect(img2, keyPoints2);

	cv::Mat imgFeatPoints1, imgFeatPoints2;
	cv::drawKeypoints(img1, keyPoints1,imgFeatPoints1, cv::Scalar(255, 255, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	cv::drawKeypoints(img2, keyPoints2,imgFeatPoints2, cv::Scalar(255, 255, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

	cv::Mat descriptorImg1, descriptorImg2;
	extractor.compute(img1, keyPoints1, descriptorImg1);
	extractor.compute(img2, keyPoints2, descriptorImg2);

	std::vector<std::vector<cv::DMatch>> matches(cv::NORM_L2);
	cv::BFMatcher bfMatcher;
	bfMatcher.knnMatch(descriptorImg1, descriptorImg2, matches, 2);
	std::vector<cv::Point2f> featurePoints1, featurePoints2;
	std::vector<cv::DMatch> goodMatches;
	std::vector<cv::DMatch> goodMatches1;
	cv::Mat imgForShowPoints1 = img1.clone();
	cv::Mat imgForShowPoints2 = img2.clone();
	for (int i = 0; i < matches.size(); ++i) {
		const float ratio = (float)0.6/*9*/;
		if ( matches[i][0].distance < ratio * matches[i][1].distance ) {
			int index1 = matches[i][0].trainIdx;
			int index2 = matches[i][0].queryIdx;
			cv::Point2f tmpKeyPoint1 = keyPoints1[index1].pt;
			cv::Point2f tmpKeyPoint2 = keyPoints2[index2].pt;
			
		//	float distanceBetPoints = std::abs(tmpKeyPoint1.y - tmpKeyPoint2.y(;
			float diffBetweenXCoord = std::abs(tmpKeyPoint1.x - tmpKeyPoint2.x);
			float diffBetweenYCoord = std::abs(tmpKeyPoint1.y - tmpKeyPoint2.y);
		//	float distanceBetPoints = std::sqrtf((tmpKeyPoint1.x - tmpKeyPoint2.x)*(tmpKeyPoint1.x - tmpKeyPoint2.x) + (tmpKeyPoint1.y - tmpKeyPoint2.y)*(tmpKeyPoint1.y - tmpKeyPoint2.y));
		//	float distanceBetPoints = std::abs(tmpKeyPoint2.y - tmpKeyPoint1.y);
			if (diffBetweenXCoord <= 8 && diffBetweenYCoord <= 8  && tmpKeyPoint1.y > 20 && tmpKeyPoint2.y > 20 && tmpKeyPoint1.y < img1.rows - 20 && tmpKeyPoint2.y < img1.rows - 20 
				&& tmpKeyPoint1.x > 20 && tmpKeyPoint2.x > 20 && tmpKeyPoint1.x < img1.cols - 20 && tmpKeyPoint2.x < img1.cols - 20   )  {
				cv::circle(imgForShowPoints1, tmpKeyPoint1, 2, cv::Scalar(255, 0, 0));
				cv::circle(imgForShowPoints2, tmpKeyPoint2, 2, cv::Scalar(255, 0, 0));
				cv::imshow("current image with points1", imgForShowPoints1);
				cv::imshow("current image with points2", imgForShowPoints2);
				std::cout << "TMP POINT INFO " << tmpKeyPoint1.x << "  " << tmpKeyPoint1.y << "   " << tmpKeyPoint2.x << "  " << tmpKeyPoint2.y << std::endl;
				cv::waitKey(10);
				
				
				
		//		std::cout << "ASASA" << std::endl;
		//		std::cout << "Tmp Key points  " <<tmpKeyPoint1.x << "  " << tmpKeyPoint1.y  <<"   "<<tmpKeyPoint2.x<<"   "<<tmpKeyPoint2.y<< std::endl;
		//		std::cout << "Index 1 " << index1 << "   " << index2 << std::endl;
				//std::cout << "Distance " << distanceBetPoints << std::endl;
				
				goodMatches.push_back(matches[i][0]);
		    }
			
		}
	}
	
	

	for (int i = 0; i < goodMatches.size(); ++i) {
		featurePoints1.push_back(keyPoints1[goodMatches[i].queryIdx].pt);
		featurePoints2.push_back(keyPoints2[goodMatches[i].trainIdx].pt);
	}
	cv::Mat imgMatches;
	cv::Mat outputMask;
	
	cv::Mat H = cv::findHomography(featurePoints1, featurePoints2, outputMask,CV_FM_RANSAC_ONLY);
	std::cout << "Output mask size " << outputMask.rows<< std::endl;
	std::cout << "good matches size " << goodMatches.size() << std::endl;
	for (int i = 0; i < outputMask.rows; ++i) {
	//	for (int j = 0; j < outputMask.cols; ++j) {
		int currValue = (int)outputMask.at<uchar>(i, 0);
		std::cout <<currValue << "  ";
		if (currValue == 1){
			goodMatches1.push_back(goodMatches.at(i));
		}
		std::cout<<std::endl;
	}
	featurePoints1.clear();
	featurePoints2.clear();
	for (int i = 0; i < goodMatches1.size(); ++i) {
		featurePoints1.push_back(keyPoints1[goodMatches1[i].queryIdx].pt);
		featurePoints2.push_back(keyPoints2[goodMatches1[i].trainIdx].pt);
	}

cv::Mat fundamentalMatrix = cv::findFundamentalMat(featurePoints1, featurePoints2, CV_FM_RANSAC);
	

	
	/*feauturePointsRes =  getFeauturePoints(img1, img2);
	for (int i = 0; i < feauturePointsRes.first.size(); ++i) {
		(feauturePointsRes.first.at(i).x) = (float)((float)(feauturePointsRes.first.at(i).x) / (float)(img1.cols));
		(feauturePointsRes.first.at(i).y) = (float)((float)(feauturePointsRes.first.at(i).y) / (float)(img1.rows));
		(feauturePointsRes.second.at(i).x) = (float)((float)(feauturePointsRes.second.at(i).x) / (float)(img1.cols));
		(feauturePointsRes.second.at(i).y) = (float)((float)(feauturePointsRes.second.at(i).y) / (float)(img1.rows));
	}
	cv::Mat fundamentalMatrix = cv::findFundamentalMat(feauturePointsRes.first, feauturePointsRes.second,CV_FM_RANSAC);
	*/

	
	cv::Mat essentialMatrix = K.t() * fundamentalMatrix * K;
	
/*	cv::Mat essentialMatrix(3, 3, CV_64F);
	essentialMatrix.at<double>(0, 0) = 0.1376;
	essentialMatrix.at<double>(0, 1) = 3.3418;
	essentialMatrix.at<double>(0, 2) = 0.8015;
	essentialMatrix.at<double>(1, 0) = -3.2499;
	essentialMatrix.at<double>(1, 1) = 0.1794;
	essentialMatrix.at<double>(1, 2) = 1.0408;
	essentialMatrix.at<double>(2, 0) = -1.6025;
	essentialMatrix.at<double>(2, 1) =  -0.9170;
	essentialMatrix.at<double>(2, 2) = 0.0940;*/


	

	std::cout << "Fundamental  Matrix " << std::endl << fundamentalMatrix << std::endl;
	cv::SVD decomp = cv::SVD(essentialMatrix);
	cv::Matx33d W(0, -1, 0,
		1, 0, 0,
		0, 0, 1);
	cv::Matx33d Wt(0, 1, 0,
		-1, 0, 0,
		0, 0, 1);
	cv::Mat R1 = decomp.u * cv::Mat(W) * decomp.vt;
	cv::Mat R2 = decomp.u * cv::Mat(Wt) * decomp.vt;
	cv::Mat t1 = decomp.u.col(2); //u3
	cv::Mat t2 = -decomp.u.col(2); //u3
	//  cv::Mat t2 = decomp.vt.col(2); //u3
	//cv::Mat_ R3 =  decomp.u * cv::Mat(Wt) * decomp.vt;
//	Eigen::Matrix4f resultMat;
	//cv::Matx34d resultMat;
	resultMat(0, 0) =  -R1.at<double>(0, 0);
	resultMat(0, 1) =  R1.at<double>(0, 1);
	resultMat(0, 2) =  R1.at<double>(0, 2);
	resultMat(1, 0) =  R1.at<double>(1, 0);
	resultMat(1, 1) =  -R1.at<double>(1, 1);
	resultMat(1, 2) =  R1.at<double>(1, 2);
	resultMat(2, 0) =  R1.at<double>(2, 0);
	resultMat(2, 1) =  R1.at<double>(2, 1);
	resultMat(2, 2) =  -R1.at<double>(2, 2);
	resultMat(0, 3) =  t1.at<double>(0, 0);
	resultMat(1, 3) =  t1.at<double>(0, 1);
	resultMat(2, 3) =   t1.at<double>(0, 2);

	




	

	// cv::Matx33d rotationMatrix = R1;
	//cv::Matx13d translationMatrix = t1;

	std::ofstream myFile;
	myFile.open("RandT.txt");
	myFile << "Rotation matrix  \n";
	myFile << R1 << "\n";
	myFile << "Translation matrix \n";
	myFile << t1;
	myFile.close();

	std::cout << "Rotation 1  Our Method " << std::endl << R1 << std::endl;
	std::cout << "Translation 1  Our Method" << std::endl << t1 << std::endl;
	cv::drawMatches(img1, keyPoints1, img2, keyPoints2, goodMatches1, imgMatches, cv::Scalar::all(-1), cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
	//cv::imshow("Mask", outputMask);
	//cv::imshow("Img1 ", img1);
	//cv::imshow("Img2", img2);
	//cv::imshow("ImgMatches", imgMatches);
	cv::imwrite("goodMatches2.png", imgMatches);
	cv::waitKey(0);

}


void findRotAndTransMatrixesWithTemplateMatch(const cv::Mat& img1, const cv::Mat& img2, const cv::Size& rectSize, Eigen::Matrix4f& resultMat, std::vector<double>& frameRTInfo)
{

	cv::Mat cameraMatrixMat = cv::Mat(3, 3, CV_64F);
	cameraMatrixMat.at<double>(0, 0) = 2.1984690542080136e+002;
	cameraMatrixMat.at<double>(0, 1) = 0.;
	cameraMatrixMat.at<double>(0, 2) = 1.1422996755695458e+002;
	cameraMatrixMat.at<double>(1, 0) = 0.;
	cameraMatrixMat.at<double>(1, 1) = 2.1907427404038518e+002;
	cameraMatrixMat.at<double>(1, 2) = 1.3339885990322043e+002;
	cameraMatrixMat.at<double>(2, 0) = 0.;
	cameraMatrixMat.at<double>(2, 1) = 0.;
	cameraMatrixMat.at<double>(2, 2) = 1.;

	cv::Mat K = cv::Mat(3, 3, CV_64F);
	K.at<double>(0, 0) = 237.46;
	K.at<double>(0, 1) = 0;
	K.at<double>(0, 2) = 0;
	K.at<double>(1, 0) = 0;
	K.at<double>(1, 1) = 237.15;
	K.at<double>(1, 2) = 0;
	K.at<double>(2, 0) = 160.35;
	K.at<double>(2, 1) = 137.6620;
	K.at<double>(2, 2) = 1;



	correspondPoints featurePointsVec;
	std::vector<cv::Point2f> oppositePoints;
	LocalTemplateMatching(img1, img2,rectSize, featurePointsVec,oppositePoints);

	std::vector<cv::Point2f> featuresVec1;
	std::vector<cv::Point2f> featuresVec2;
	for (int i = 0; i < featurePointsVec.size(); ++i) {
		
		featuresVec1.push_back(featurePointsVec.at(i).first);
		featuresVec2.push_back(featurePointsVec.at(i).second);
	}
	for (int g = 0; g < featuresVec1.size(); ++g) {
		std::cout << "Point1 " << featuresVec1.at(g).x << "  " << featuresVec1.at(g).y << std::endl;

		std::cout << "Point2  " << featuresVec2.at(g).x << "  " << featuresVec2.at(g).y << std::endl;
	}
	cv::Mat fundamentalMatrix = cv::findFundamentalMat(featuresVec1, featuresVec2, CV_FM_RANSAC);
	std::cout << "Fundamental matrix " << fundamentalMatrix << std::endl;
	cv::Mat essentialMatrix = cameraMatrixMat.t() * fundamentalMatrix * cameraMatrixMat;
	cv::SVD decomp = cv::SVD(essentialMatrix);
	cv::Matx33d W(0, -1, 0,
		1, 0, 0,
		0, 0, 1);
	cv::Matx33d Wt(0, 1, 0,
		-1, 0, 0,
		0, 0, 1);
	cv::Mat R1 = decomp.u * cv::Mat(W) * decomp.vt;
	cv::Mat R2 = decomp.u * cv::Mat(Wt) * decomp.vt;
	cv::Mat t1 = decomp.u.col(2); //u3
	cv::Mat t2 = -decomp.u.col(2); //u3

	std::cout << "Rotation 1  Our Method " << std::endl << R1 << std::endl;
	std::cout << "Translation 1  Our Method" << std::endl << t1 << std::endl;
	double X, Y, Z;
	calculateAnglesFromRotMatrix(R1, X, Y, Z);
	
	float3x3 tmpRot;
	tmpRot[0][0] = R1.at<float>(0,0);
	tmpRot[0][1] = R1.at<float>(0,1);
	tmpRot[0][2] = R1.at<float>(0,2);
	tmpRot[1][0] = R1.at<float>(1,0);
	tmpRot[1][1] = R1.at<float>(1,1);
	tmpRot[1][2] = R1.at<float>(1,2);
	tmpRot[2][0] = R1.at<float>(2,0);
	tmpRot[2][1] = R1.at<float>(2,1);
	tmpRot[2][2] = R1.at<float>(2,2);
	float3 angles = eulerAngles(tmpRot);
	frameRTInfo.push_back(X);
	frameRTInfo.push_back(Y);
	frameRTInfo.push_back(Z);
	frameRTInfo.push_back(t1.at<double>(0,0));
	frameRTInfo.push_back(t1.at<double>(0, 1));
	frameRTInfo.push_back(t1.at<double>(0, 2));


	//std::cout << "ANGLES FOR ROT MATIX " <<double (angles[0]) << "  " <<double( angles[1]) << "   " <<double( angles[2]) << std::endl;
	//std::cout << "ANGLES FOR ROT MATIX Old " <<X << "  " << Y << "   " << Z << std::endl;



}

void calculateAnglesFromRotMatrix(const cv::Mat& rotMatrix, double& X, double& Y, double& Z)
{

	X = std::atan2(rotMatrix.at<double>(2, 1), rotMatrix.at<double>(2, 2));
	Y = std::atan2(-rotMatrix.at<double>(2, 0), std::sqrt(std::pow(rotMatrix.at<double>(2, 1),2.0) + std::pow(rotMatrix.at<double>(2, 2),2.0)));
	Z = std::atan2(rotMatrix.at<double>(1, 0), rotMatrix.at<double>(0, 0));

/*	X =  std::atan2(rotMatrix.at<double>(3, 2), rotMatrix.at<double>(3, 3));
	Y = std::atan2(-rotMatrix.at<double>(3, 1), sqrt(rotMatrix.at<double>(3, 2)*rotMatrix.at<double>(3, 2) + rotMatrix.at<double>(3, 3)*rotMatrix.at<double>(3, 3)));
	Z = std::atan2(rotMatrix.at<double>(2, 1), rotMatrix.at<double>(1, 1));
	*/
	/*Y = std::atan2(rotMatrix.at<double>(2,1), rotMatrix.at<double>(2,2));
	X = std::asin(rotMatrix.at<double>(2,0));
	Z = -std::atan2(rotMatrix.at<double>(1,0), rotMatrix.at<double>(0,0));
	*/

	/*X = std::atan2(-rotMatrix.at<double>(2, 0), rotMatrix.at<double>(0, 0));
	Y = std::atan2(-rotMatrix.at<double>(1, 2), rotMatrix.at<double>(1, 1));
	Z = std::asin(rotMatrix.at<double>(1, 0));*/

/*	X = std::atan2(rotMatrix.at<double>(2, 0), rotMatrix.at<double>(2, 1));
	Y = std::acos(rotMatrix.at<double>(2, 2));
	Z = -std::atan2(rotMatrix.at<double>(0, 2), rotMatrix.at<double>(1, 2));*/


}


bool closeEnough(const float& a, const float& b, const float& epsilon1) {
	return (epsilon1 > std::abs(a - b));
}

float3 eulerAngles(const float3x3& R) {

	float alphaAng;
	float gamma;
	float betta = std::atan2(-R[2][0], std::sqrtf(std::powf(R[0][0], 2) + std::powf(R[1][0], 2)));
	if (closeEnough(betta, M_PI, 1)) {
		alphaAng = 0;
		gamma = std::atan2(R[0][1], R[1][1]);
	}
	else if (closeEnough(betta, -M_PI, 1)) {
		alphaAng = 0;
		gamma = -std::atan2(R[0][1], R[1][1]);
	}
	else {
		alphaAng = std::atan2(R[1][0] / std::cosf(betta), R[0][0] / std::cos(betta));
		gamma =  std::atan2(R[2][1] / std::cosf(betta), R[2][2] / std::cos(betta));
	}

	return { betta, gamma, alphaAng };
	//check for gimbal lock
	/*if (closeEnough(R[0][2], -1.0f)) {
		float x = 0; //gimbal lock, value of x doesn't matter
		float y = PI / 2;
		float z = x + atan2(R[1][0], R[2][0]);
		return { x, y, z };
	}
	else if (closeEnough(R[0][2], 1.0f)) {
		float x = 0;
		float y = -PI / 2;
		float z = -x + atan2(-R[1][0], -R[2][0]);
		return{ x, y, z };
	}
	else { //two solutions exist
		std::cout << "Third case" << std::endl;
		float x1 = -asin(R[0][2]);
		float x2 = PI - x1;

		float y1 = atan2(R[1][2] / cos(x1), R[2][2] / cos(x1));
		float y2 = atan2(R[1][2] / cos(x2), R[2][2] / cos(x2));

		float z1 = atan2(R[0][1] / cos(x1), R[0][0] / cos(x1));
		float z2 = atan2(R[0][1] / cos(x2), R[0][0] / cos(x2));

		//choose one solution to return
		//for example the "shortest" rotation
		if ((std::abs(x1) + std::abs(y1) + std::abs(z1)) <= (std::abs(x2) + std::abs(y2) + std::abs(z2))) {
			return{ x1, y1, z1 };
		}
		else {
			return{ x2, y2, z2 };
		}
	}*/
}

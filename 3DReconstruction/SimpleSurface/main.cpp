/*
#include <iostream>
#include <stdio.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "templateMatching.h"





int main()
{
	/// Load image and template
	cv::Mat img = cv::imread("D:\\upWorkProjs\\StomatchVideos\\punkteImages\\punkteImage334.png");
    cv::Mat templ =cv::imread("D:\\upWorkProjs\\StomatchVideos\\punkteImages\\punkteImage333.png");

	
	//templ = imread(argv[2], 1);

	cv::Rect cropRect = cv::Rect(20, 20, img.cols-50, img.rows-50);
	templ = templ(cropRect);

	cv::imshow("Templ image", templ);
	cv::imshow("Source image", img);
	cv::waitKey(0);

	/// Create Trackbar
	//char* trackbar_label = "Method: \n 0: SQDIFF \n 1: SQDIFF NORMED \n 2: TM CCORR \n 3: TM CCORR NORMED \n 4: TM COEFF \n 5: TM COEFF NORMED";
	//createTrackbar(trackbar_label, image_window, &match_method, max_Trackbar, MatchingMethod);

	MatchingMethod(img, templ);

	//waitKey(0);
	return 0;
}*/











/*
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/io/ply_io.h>

using namespace std;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

void
printHelp(int, char **argv)
{
	print_error("Syntax is: %s input.xyz output.pcd\n", argv[0]);
}
*//*
bool
loadCloud(const string &filename, PointCloud<PointXYZ> &cloud)
{
	ifstream fs;
	fs.open(filename.c_str(), ios::binary);
	if (!fs.is_open() || fs.fail())
	{
		PCL_ERROR("Could not open file '%s'! Error : %s\n", filename.c_str(), strerror(errno));
		fs.close();
		return (false);
	}

	string line;
	vector<string> st;

	while (!fs.eof())
	{
		getline(fs, line);
		// Ignore empty lines
		if (line == "")
			continue;

		// Tokenize the line
		boost::trim(line);
		boost::split(st, line, boost::is_any_of("\t\r "), boost::token_compress_on);

		if (st.size() != 3)
			continue;

		cloud.push_back(PointXYZ(float(atof(st[0].c_str())), float(atof(st[1].c_str())), float(atof(st[2].c_str()))));
	}
	fs.close();

	cloud.width = uint32_t(cloud.size()); cloud.height = 1; cloud.is_dense = true;
	return (true);
}
*/
/*
int main(int argc, char** argv)
{
	print_info("Convert a simple XYZ file to PCD format. For more information, use: %s -h\n", argv[0]);

	if (argc < 3)
	{
		printHelp(argc, argv);
		return (-1);
	}

	// Parse the command line arguments for .pcd and .ply files
	vector<int> pcd_file_indices = parse_file_extension_argument(argc, argv, ".pcd");
	vector<int> xyz_file_indices = parse_file_extension_argument(argc, argv, ".xyz");
	if (pcd_file_indices.size() != 1 || xyz_file_indices.size() != 1)
	{
		print_error("Need one input XYZ file and one output PCD file.\n");
		return (-1);
	}

	// Load the first file
	PointCloud<PointXYZ> cloud;
	if (!loadCloud(argv[xyz_file_indices[0]], cloud))
		return (-1);

	// Convert to PCD and save
	pcl::PLYWriter w1;
	PCDWriter w;
	w.writeBinaryCompressed(argv[pcd_file_indices[0]], cloud);
	//pcl::io::savePLYFileBinary(argv[pcd_file_indices[0]], cloud);
}*/


#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

#include "findRotAndTransMat.h"
#include "templateMatching.h"
#include "imageUndistortion.h"
#include "csvParser.h"

#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>


using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

// This is a tutorial so we can afford having global variables 
//our visualizer
pcl::visualization::PCLVisualizer *p;
//its left and right viewports
int vp_1, vp_2;

//convenient structure to handle our pointclouds
struct PCD
{
	//PointCloud::Ptr cloud;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
	std::string f_name;

//	PCD() : cloud(new PointCloud) {};
	PCD() : cloud(new pcl::PointCloud<pcl::PointXYZRGB>) {};
};

struct PCDComparator
{
	bool operator () (const PCD& p1, const PCD& p2)
	{
	
		return (p1.f_name < p2.f_name);
	}
};

bool loadCloud(const std::string &filename, PointCloud &cloud)
{
	ifstream fs;
	fs.open(filename.c_str(), ios::binary);
	if (!fs.is_open() || fs.fail())
	{
		PCL_ERROR("Could not open file '%s'! Error : %s\n", filename.c_str(), strerror(errno));
		fs.close();
		return (false);
	}

	std::string line;
	std::vector<std::string> st;

	while (!fs.eof())
	{
		getline(fs, line);
		// Ignore empty lines
		if (line == "")
			continue;

		// Tokenize the line
		boost::trim(line);
		boost::split(st, line, boost::is_any_of("\t\r "), boost::token_compress_on);

		if (st.size() != 3)
			continue;

		cloud.push_back(pcl::PointXYZ(float(atof(st[0].c_str())), float(atof(st[1].c_str())), float(atof(st[2].c_str()))));
	}
	fs.close();

	cloud.width = uint32_t(cloud.size()); cloud.height = 1; cloud.is_dense = true;
	return (true);
}
// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
	using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
	MyPointRepresentation()
	{
		// Define the number of dimensions
		nr_dimensions_ = 4;
	}

	// Override the copyToFloatArray method to define our feature vector
	virtual void copyToFloatArray(const PointNormalT &p, float * out) const
	{
		// < x, y, z, curvature >
		out[0] = p.x;
		out[1] = p.y;
		out[2] = p.z;
		out[3] = p.curvature;
	}
};


////////////////////////////////////////////////////////////////////////////////





void showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source)
{
	p->removePointCloud("vp1_target");
	p->removePointCloud("vp1_source");

	PointCloudColorHandlerCustom<PointT> tgt_h(cloud_target, 0, 255, 0);
	PointCloudColorHandlerCustom<PointT> src_h(cloud_source, 255, 0, 0);
	p->addPointCloud(cloud_target, tgt_h, "vp1_target", vp_1);
	p->addPointCloud(cloud_source, src_h, "vp1_source", vp_1);

	PCL_INFO("Press q to begin the registration.\n");
	p->spin();
}


////////////////////////////////////////////////////////////////////////////////

void showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source)
{
	p->removePointCloud("source");
	p->removePointCloud("target");


	PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler(cloud_target, "curvature");
	if (!tgt_color_handler.isCapable())
		PCL_WARN("Cannot create curvature color handler!");

	PointCloudColorHandlerGenericField<PointNormalT> src_color_handler(cloud_source, "curvature");
	if (!src_color_handler.isCapable())
		PCL_WARN("Cannot create curvature color handler!");


	p->addPointCloud(cloud_target, tgt_color_handler, "target", vp_2);
	p->addPointCloud(cloud_source, src_color_handler, "source", vp_2);

	p->spinOnce();
}

////////////////////////////////////////////////////////////////////////////////

void loadData(int argc, char **argv, std::vector<PCD, Eigen::aligned_allocator<PCD> > &models)
{
	std::string extension(".pcd");
	// Suppose the first argument is the actual test model
	int countOfFilesInDir =  std::atoi(argv[3]);
	std::cout << "Count of files in folder  " << argv[0] << "  " << argv[1] <<"  " <<argv[2]<<"  "<<countOfFilesInDir<< std::endl;
	std::string dirPath = std::string(argv[1]);
	std::string fileMainPart = std::string(argv[2]);


//	for (int i = 1; i < argc; i++)
	for (int i = 1; i <= countOfFilesInDir; ++i)
	{
	//	std::string fname = std::string(argv[i]);
		std::string fname = dirPath + "\\" + fileMainPart  + std::to_string((long int)(i*5)) +  "Colored.pcd";
		std::string fname1 = fname;
		std::cout << "FILE PATH  " << fname1 << std::endl;
  		// Needs to be at least 5: .plot
		if (fname.size() <= extension.size())
			continue;
		
		std::transform(fname1.begin(), fname1.end(), fname1.begin(), (int(*)(int))tolower);

		//check that the argument is a pcd file
		if (fname1.compare(fname1.size() - extension.size(), extension.size(), extension) == 0)
		{
			// Load the cloud and saves it into the global list of models
			PCD m;
		//m.f_name = argv[i];
			m.f_name = fname;
			//pcl::io::loadPCDFile(argv[i], *m.cloud);
			pcl::io::loadPCDFile(fname, *m.cloud);
			//remove NAN points from the cloud
			std::vector<int> indices;
			pcl::removeNaNFromPointCloud(*m.cloud, *m.cloud, indices);
			models.push_back(m);
		}
	}
}


////////////////////////////////////////////////////////////////////////////////

void pairAlignOld(const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform,Eigen::Matrix4f resultMat )
{
	//
	// Downsample for consistency and speed
	// \note enable this for large datasets
	PointCloud::Ptr src(new PointCloud);
	PointCloud::Ptr tgt(new PointCloud);
	pcl::VoxelGrid<PointT> grid;
	if (true)
	{
		grid.setLeafSize(0.05, 0.05, 0.05);
		grid.setInputCloud(cloud_src);
		grid.filter(*src);

		grid.setInputCloud(cloud_tgt);
		grid.filter(*tgt);
	}
	else
	{
		src = cloud_src;
		tgt = cloud_tgt;
	}


	// Compute surface normals and curvature
	PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
	PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

	pcl::NormalEstimation<PointT, PointNormalT> norm_est;
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
	norm_est.setSearchMethod(tree);
	norm_est.setKSearch(30);

	norm_est.setInputCloud(src);
	norm_est.compute(*points_with_normals_src);
	pcl::copyPointCloud(*src, *points_with_normals_src);

	norm_est.setInputCloud(tgt);
	norm_est.compute(*points_with_normals_tgt);
	pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

	//
	// Instantiate our custom point representation (defined above) ...
	MyPointRepresentation point_representation;
	// ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
	float alpha[4] = { 1.0, 1.0, 1.0, 1.0 };
	point_representation.setRescaleValues(alpha);

	//
	// Align
	pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
	reg.setTransformationEpsilon(1e-6);
	// Set the maximum distance between two correspondences (src<->tgt) to 10cm
	// Note: adjust this based on the size of your datasets
	reg.setMaxCorrespondenceDistance(0.1);
	// Set the point representation
	reg.setPointRepresentation(boost::make_shared<const MyPointRepresentation>(point_representation));

	reg.setInputSource(points_with_normals_src);
	reg.setInputTarget(points_with_normals_tgt);



	//
	// Run the same optimization in a loop and visualize the results
	Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
	PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
	reg.setMaximumIterations(2);
	for (int i = 0; i < 30; ++i)
	{
		PCL_INFO("Iteration Nr. %d.\n", i);

		// save cloud for visualization purpose
		points_with_normals_src = reg_result;

		// Estimate
		reg.setInputSource(points_with_normals_src);
		reg.align(*reg_result);
		
		//accumulate transformation between each Iteration
	    Ti = reg.getFinalTransformation() * Ti;
		
	//	targetToSource = resultMat;
	//	resultMat = Ti;
		//if the difference between this transformation and the previous one
		//is smaller than the threshold, refine the process by reducing
		//the maximal correspondence distance
		Eigen::Matrix4f matrixOfRotTr = targetToSource;
		std::cout << "Matrix elements" << std::endl;
		//	  myFile<<matrixOfRotTr<<"\n";
		std::cout << matrixOfRotTr(0, 0) << "  " << matrixOfRotTr(0, 1) << "  " << matrixOfRotTr(0, 2) << "  " << matrixOfRotTr(0, 3) << std::endl;;
		std::cout << matrixOfRotTr(1, 0) << "  " << matrixOfRotTr(1, 1) << "  " << matrixOfRotTr(1, 2) << "  " << matrixOfRotTr(1, 3) << std::endl;;
		std::cout << matrixOfRotTr(2, 0) << "  " << matrixOfRotTr(2, 1) << "  " << matrixOfRotTr(2, 2) << "  " << matrixOfRotTr(2, 3) << std::endl;;
		std::cout << matrixOfRotTr(3, 0) << "  " << matrixOfRotTr(3, 1) << "  " << matrixOfRotTr(3, 2) << "  " << matrixOfRotTr(3, 3) << std::endl;;
		if (fabs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon())
			reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - 0.001);

		prev = reg.getLastIncrementalTransformation();

		// visualize current state
		showCloudsRight(points_with_normals_tgt, points_with_normals_src);
	}

	//
	// Get the transformation from target to source
	targetToSource = Ti.inverse();
	//targetToSource = targetToSource.inverse();
	//
	// Transform target back in source frame
	pcl::transformPointCloud(*cloud_tgt, *output, targetToSource);

	p->removePointCloud("source");
	p->removePointCloud("target");

	PointCloudColorHandlerCustom<PointT> cloud_tgt_h(output, 0, 255, 0);
	PointCloudColorHandlerCustom<PointT> cloud_src_h(cloud_src, 255, 0, 0);
	p->addPointCloud(output, cloud_tgt_h, "target", vp_2);
	p->addPointCloud(cloud_src, cloud_src_h, "source", vp_2);

	
	PCL_INFO("Press q to continue the registration.\n");
	p->spin();

	p->removePointCloud("source");
	p->removePointCloud("target");

	//add the source to the transformed target
	*output += *cloud_src;

	final_transform = targetToSource;
}


void pairAlign(const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform, Eigen::Matrix4f resultMat, bool downsample = false)
{
	//
	// Downsample for consistency and speed
	// \note enable this for large datasets
	PointCloud::Ptr src(new PointCloud);
	PointCloud::Ptr tgt(new PointCloud);
	pcl::VoxelGrid<PointT> grid;
	if (downsample)
	{
		grid.setLeafSize(0.05, 0.05, 0.05);
		grid.setInputCloud(cloud_src);
		grid.filter(*src);

		grid.setInputCloud(cloud_tgt);
		grid.filter(*tgt);
	}
	else
	{
		src = cloud_src;
		tgt = cloud_tgt;
	}


	// Compute surface normals and curvature
	PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
	PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

	pcl::NormalEstimation<PointT, PointNormalT> norm_est;
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
	norm_est.setSearchMethod(tree);
	norm_est.setKSearch(30);

	norm_est.setInputCloud(src);
	norm_est.compute(*points_with_normals_src);
	pcl::copyPointCloud(*src, *points_with_normals_src);

	norm_est.setInputCloud(tgt);
	norm_est.compute(*points_with_normals_tgt);
	pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

	//
	// Instantiate our custom point representation (defined above) ...
	MyPointRepresentation point_representation;
	// ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
	float alpha[4] = { 1.0, 1.0, 1.0, 1.0 };
	point_representation.setRescaleValues(alpha);

	//
	// Align
	pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
	reg.setTransformationEpsilon(1e-6);
	// Set the maximum distance between two correspondences (src<->tgt) to 10cm
	// Note: adjust this based on the size of your datasets
	reg.setMaxCorrespondenceDistance(0.1);
	// Set the point representation
	reg.setPointRepresentation(boost::make_shared<const MyPointRepresentation>(point_representation));

	reg.setInputSource(points_with_normals_src);
	reg.setInputTarget(points_with_normals_tgt);



	//
	// Run the same OPTIMIZATION in a loop and visualize the results
	Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
	PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
	reg.setMaximumIterations(2);
	for (int i = 0; i < 30; ++i)
	{
	//	PCL_INFO("Iteration Nr. %d.\n", i);

		// save cloud for visualization purpose
		points_with_normals_src = reg_result;

		// Estimate
		reg.setInputSource(points_with_normals_src);
		reg.align(*reg_result);

		//accumulate transformation between each Iteration
		Ti = reg.getFinalTransformation() * Ti;

		//if the difference between this transformation and the previous one
		//is smaller than the threshold, refine the process by reducing
		//the maximal correspondence distance
		if (fabs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon())
			reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - 0.001);

		prev = reg.getLastIncrementalTransformation();

		// visualize current state
		showCloudsRight(points_with_normals_tgt, points_with_normals_src);
	}
	Ti = resultMat;
	//
	// Get the transformation from target to source
	targetToSource = Ti.inverse();

	//
	// Transform target back in source frame
	pcl::transformPointCloud(*cloud_tgt, *output, targetToSource);

	p->removePointCloud("source");
	p->removePointCloud("target");

	PointCloudColorHandlerCustom<PointT> cloud_tgt_h(output, 0, 255, 0);
	PointCloudColorHandlerCustom<PointT> cloud_src_h(cloud_src, 255, 0, 0);
	p->addPointCloud(output, cloud_tgt_h, "target", vp_2);
	p->addPointCloud(cloud_src, cloud_src_h, "source", vp_2);

	PCL_INFO("Press q to continue the registration.\n");
	p->spin();

	p->removePointCloud("source");
	p->removePointCloud("target");

	//add the source to the transformed target
	*output += *cloud_src;

	final_transform = targetToSource;
}

void generateColoredPointCloud(const std::string& inputCloudPath, const std::string& inputImagePath, const std::string& outputCloudPath)
{
	pcl::PointCloud<pcl::PointXYZ> cloudWithoutColor;
	cv::Mat img = cv::imread(inputImagePath);
	pcl::PointCloud<pcl::PointXYZRGB> cloudWithColor;
	std::cout << "Point cloud data" << inputCloudPath << "  " << outputCloudPath << "  " << inputImagePath << std::endl;
	pcl::io::loadPCDFile(inputCloudPath, cloudWithoutColor);
	cloudWithColor.resize(cloudWithoutColor.width*cloudWithoutColor.height);
	for (int i = 0; i < cloudWithoutColor.height; ++i){
		for (int j = 0; j < cloudWithoutColor.width; ++j){
			cloudWithColor.points[i*cloudWithoutColor.width + j].x = cloudWithoutColor.points[i*cloudWithoutColor.width + j].x;
			cloudWithColor.points[i*cloudWithoutColor.width + j].y = cloudWithoutColor.points[i*cloudWithoutColor.width + j].y;
			cloudWithColor.points[i*cloudWithoutColor.width + j].z = cloudWithoutColor.points[i*cloudWithoutColor.width + j].z;
			cloudWithColor.points[i*cloudWithoutColor.width + j].b = img.at<cv::Vec3b>(i, j)[0];
			cloudWithColor.points[i*cloudWithoutColor.width + j].g = img.at<cv::Vec3b>(i, j)[1];
			cloudWithColor.points[i*cloudWithoutColor.width + j].r = img.at<cv::Vec3b>(i, j)[2];
		}
	}

	pcl::io::savePCDFileBinary(outputCloudPath, cloudWithColor);
	return ;

}





void gammaCorrection(const cv::Mat& inputImg,  double powerValue, cv::Mat& outputImg)
{
	outputImg = cv::Mat(inputImg.rows, inputImg.cols, CV_8UC3);
	cv::Mat imgFloat = cv::Mat(inputImg.rows, inputImg.cols, CV_64FC3);
	for (int i = 0; i < inputImg.rows; ++i) {
		for (int j = 0; j < inputImg.cols; ++j) {
			imgFloat.at<cv::Vec3d>(i, j)[0] = (float)inputImg.at<cv::Vec3b>(i, j)[0] / (float)255.0;
			imgFloat.at<cv::Vec3d>(i, j)[1] = (float)inputImg.at<cv::Vec3b>(i, j)[1] / (float)255.0;
			imgFloat.at<cv::Vec3d>(i, j)[2] = (float)inputImg.at<cv::Vec3b>(i, j)[2] / (float)255.0;
		}
	}
	cv::pow(imgFloat, powerValue, imgFloat);
	for (int i = 0; i < inputImg.rows; ++i) {
		for (int j = 0; j < inputImg.cols; ++j) {
		
			outputImg.at<cv::Vec3b>(i, j)[0] = imgFloat.at<cv::Vec3d>(i, j)[0] * 255;
			outputImg.at<cv::Vec3b>(i, j)[1] = imgFloat.at<cv::Vec3d>(i, j)[1] * 255;
			outputImg.at<cv::Vec3b>(i, j)[2] = imgFloat.at<cv::Vec3d>(i, j)[2] * 255;
		
		}
	}

}




int mainVideoExtract()
{


	cv::VideoCapture cap1;
	cap1.open("D:\\upWorkProjs\\StereoVsionSimple\\Sen[0]17-02-2016 15-27-02.avi");
	if (!cap1.isOpened()) {
		std::cout << "Cannot111111111111 open video file " << std::endl;
		return -1;

	}
	cv::Mat initMat;

	int countIndex = 0;

	while (1)

	{

		cv::Mat frame;

		bool success = cap1.read(frame);

		if (!success){

			std::cout << "Cannot read  frame " << endl;

			break;

		}

		cv::imshow("MyVideo", frame);



		std::string imgPath1 = "D:\\upWorkProjs\\StereoVsionSimple\\StereoVsionSimple\\chessboardImages\\chessBoardNew\\" /*+ (std::string)"unitelImages\\"*/ + std::to_string((long int)(countIndex)) + ".png";

		//std::string imgPath2 = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\chessboard\\right\\" + (std::string)"rightStomachImage" + std::to_string((long double)(countIndex)) + ".ppm";

		cv::imwrite(imgPath1, frame);



		++countIndex;

		if (cv::waitKey(1) == 27) break;

	}
	return 0;






	ofstream  myfile;
	myfile.open("iamgeData.xyz");

	cv::Mat initImg = cv::imread("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage1.png");
	cv::imshow("init img", initImg);
	cv::waitKey(0);
	cv::Mat xyzImg = cv::Mat(initImg.rows, initImg.cols, CV_32FC3);

	float red, grenn, blue;
	for (int i = 0; i < initImg.rows; ++i) {
		for (int j = 0; j < initImg.cols; ++j) {
			blue = (float)((float)initImg.at<cv::Vec3b>(i,j)[0] / (float)255);
			grenn = (float)((float)initImg.at<cv::Vec3b>(i,j)[1] / (float)255);
			red = (float)((float)initImg.at<cv::Vec3b>(i,j)[2] / (float)255);

			if (red > 0.04045) { 
				
				red =  std::powf(((red + 0.055) / 1.055) , 2.4);
			}
			else {
				red = red / 12.92;
			}
			if (grenn > 0.04045){
				grenn = std::powf(((grenn + 0.055) / 1.055), 2.4);
			}
			else {
				grenn = grenn / 12.92;
			}
			if (blue > 0.04045) {
				blue = std::powf(((blue + 0.055) / 1.055) ,2.4);
			}
			else  {
				blue = blue / 12.92;
			}
		
			blue = blue * 100;
			red = red * 100;
			grenn = grenn * 100;
			
		//	xyzImg.at<cv::Vec3f>(i, j)[0] = blue* 0.4124 + grenn * 0.3576 + red * 0.1805;
		//	xyzImg.at<cv::Vec3f>(i, j)[1] = blue * 0.2126 + grenn * 0.7152 + red * 0.0722;
		//	xyzImg.at<cv::Vec3f>(i, j)[2] = blue * 0.0193 + grenn * 0.1192 + red * 0.9505;
			xyzImg.at<cv::Vec3f>(i, j)[0] = initImg.at<cv::Vec3b>(i, j)[2] * 0.5767309 + initImg.at<cv::Vec3b>(i, j)[1] * 0.1855540 + initImg.at<cv::Vec3b>(i, j)[0] * 0.1881852;
			xyzImg.at<cv::Vec3f>(i, j)[1] = initImg.at<cv::Vec3b>(i, j)[2] * 0.2973769 + initImg.at<cv::Vec3b>(i, j)[1] * 0.6273491 + initImg.at<cv::Vec3b>(i, j)[0] * 0.0752741;
			xyzImg.at<cv::Vec3f>(i, j)[2] = initImg.at<cv::Vec3b>(i, j)[2] * 0.0270343 + initImg.at<cv::Vec3b>(i, j)[1] * 0.0706872 + initImg.at<cv::Vec3b>(i, j)[0] * 0.9911085;
			myfile << xyzImg.at<cv::Vec3f>(i, j)[0] << "  " << xyzImg.at<cv::Vec3f>(i, j)[1] << "  " << xyzImg.at<cv::Vec3f>(i, j)[2] << "  ";
			myfile << '\n';
		}
		
	}
	myfile.close();
}






int main(int argc, char** argv)
{
	

	QString csvFilepath = "D:\\upWorkProjs\\StomatchVideos\\Matlab\\Matlab\\PunkteWithDistInfo.csv";
	QString csvOutFilePath = "D:\\upWorkProjs\\StomatchVideos\\Matlab\\Matlab\\PunkteWithDistInfoALLLast.csv";
//	parseCSVFile(csvFilepath,csvOutFilePath, 0, 0, 0);
//	return 0;
	cv::Mat distCoeffsMat = cv::Mat(5, 1, CV_64F);
	distCoeffsMat.at<double>(0, 0) = -3.5661442348383171e-001;
	distCoeffsMat.at<double>(1, 0) = 4.5287551599054621e-001;
	distCoeffsMat.at<double>(2, 0) = 5.0787376413176603e-003;
	distCoeffsMat.at<double>(3, 0) = 5.6453253441496895e-004;
	distCoeffsMat.at<double>(4, 0) = -1.0112793512036486e+000;

	cv::Mat cameraMatrixMat = cv::Mat(3, 3, CV_64F);
	cameraMatrixMat.at<double>(0, 0) = 1.9562370823259005e+002;
	cameraMatrixMat.at<double>(0, 1) = 0.;
	cameraMatrixMat.at<double>(0, 2) = 1.2720937280986321e+002;
	cameraMatrixMat.at<double>(1, 0) = 0.;
	cameraMatrixMat.at<double>(1, 1) = 1.9647755014883725e+002;
	cameraMatrixMat.at<double>(1, 2) = 1.3049615743476107e+002;
	cameraMatrixMat.at<double>(2, 0) = 0.;
	cameraMatrixMat.at<double>(2, 1) = 0.;
	cameraMatrixMat.at<double>(2, 2) = 1.;
	




	correspondPoints correspondingPointsVec;
	std::vector<cv::Point2f> oppositePoints;
	cv::Size rectSize = cv::Size(25, 25);
	ofstream fileInfo;
	fileInfo.open("distanceInfo.txt");
	std::string initImagePath1 = "D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage326.png";
	std::string initImagePath2 = "D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage327.png";
	std::string tmpImgPath  = "D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage";

	/*cv::Mat undistImgTmp;
	cv::Mat gammaCorMat;
	cv::Mat initMatImg = cv::imread(initImagePath1);
	gammaCorrection(initMatImg, 0.8, gammaCorMat);
	cv::undistort(gammaCorMat, undistImgTmp, cameraMatrixMat, distCoeffsMat);
	cv::imwrite("GammaUndist.png", undistImgTmp);
	return 0;*/
	
	fileInfo << "Distances  " << "X     " << "Y \n";
	std::vector<std::vector<double>> dataFramesVec;
	std::vector<double> tmpFrameInfo;
	for (int i = 240; i < 1000; ++i) {
		correspondingPointsVec.clear();
		oppositePoints.clear();
		std::string fullImgPath1 = tmpImgPath + std::to_string(long(i)) + ".png";
		std::string fullImgPath2 = tmpImgPath + std::to_string(long(i+1)) + ".png";

		cv::Mat img1 = cv::imread(fullImgPath1);
		cv::Mat img2 = cv::imread(fullImgPath2);
		cv::Mat undistImg1;
		cv::Mat undistimg2;
		//	imageUndistort(img, distCoeffsMat, cameraMatrixMat, undistgImg);
		cv::Mat diffMat1;
		cv::Mat diffMat2;
		cv::undistort(img1, undistImg1, cameraMatrixMat, distCoeffsMat);
		cv::undistort(img2, undistimg2, cameraMatrixMat, distCoeffsMat);
		cv::absdiff(undistImg1, img1, diffMat1);
		cv::absdiff(undistimg2, img2, diffMat2);
		Eigen::Matrix4f resultMatNew;
		tmpFrameInfo.clear();
		findRotAndTransMatrixesWithTemplateMatch(undistImg1, undistimg2, rectSize, resultMatNew,tmpFrameInfo);
		dataFramesVec.push_back(tmpFrameInfo);

		LocalTemplateMatching(undistImg1, undistimg2, rectSize, correspondingPointsVec, oppositePoints);
		for (int u = 0; u < correspondingPointsVec.size(); ++u){
			fileInfo << "          " << std::fabsf(oppositePoints.at(u).x - correspondingPointsVec.at(u).first.x) << "   " << std::fabsf(oppositePoints.at(u).y - correspondingPointsVec.at(u).first.y) << "\n";
			
				cv::circle(undistImg1, cv::Point(correspondingPointsVec.at(u).first.x + 30, correspondingPointsVec.at(u).first.y + 30), 2, cv::Scalar(0, 255, 0), 2);
				cv::circle(undistimg2, cv::Point(correspondingPointsVec.at(u).second.x + 30, correspondingPointsVec.at(u).second.y + 30), 2, cv::Scalar(0, 255, 0), 2);
			

		}
		cv::imshow("Img1", undistImg1);
		cv::imshow("img2", undistimg2);
		cv::waitKey(1);
		fileInfo << "\n";
	}
	fileInfo.close();
	parseCSVFile(csvFilepath, csvOutFilePath, dataFramesVec);
	return 0;
	std::cout << "Distances  " <<"X    "<<  std::endl;
	for (int i = 0; i < correspondingPointsVec.size(); ++i) {
		std::cout << "POint1 " <<(float) correspondingPointsVec.at(i).first.x << "  " << (float)correspondingPointsVec.at(i).first.y <<" Point2  " << (float)correspondingPointsVec.at(i).second.x << "  " << (float)correspondingPointsVec.at(i).second.y << std::endl;
		std::cout <<" Distance " <<std::abs(oppositePoints.at(i).x - correspondingPointsVec.at(i).first.x) << "   " << std::abs(oppositePoints.at(i).y - correspondingPointsVec.at(i).first.y) << std::endl;
	}
	return 0;

	/*cv::imwrite(initImagePath1 + "undist.png", undistImg1);
	cv::imwrite(initImagePath1 + "diff.png", diffMat1);
	cv::imwrite(initImagePath2 + "undist.png", undistimg2);
	cv::imwrite(initImagePath2 + "diff.png", diffMat2);

	for (int i = 0; i < correspondingPointsVec.size(); ++i) {
		cv::circle(img1, cv::Point(correspondingPointsVec.at(i).first.x + 30, correspondingPointsVec.at(i).first.y + 30), 2, cv::Scalar(0, 255, 0), 2);
		cv::circle(img2, cv::Point(correspondingPointsVec.at(i).second.x + 30, correspondingPointsVec.at(i).second.y + 30), 2, cv::Scalar(0, 255, 0), 2);
	}


	cv::imshow("result img1", img1);
	cv::imshow("result img2", img2);
	cv::waitKey(0);
	*/

	return 0;
}
//	cv::Mat img = cv::imread("D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage326.png");;
//	cv::Mat templ = cv::imread("D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage330.png");
//	cv::Mat img2 = cv::imread("D:\\upWorkProjs\\StomatchVideos\\punkteImages\\\punkteImage330.png");
	/*ImageUndistortor undistortor(img);	
	undistortor.setInitImg(img);
	undistortor.imageUndistort();
	img = undistortor.getUndistImg();
	undistortor.setInitImg(templ);
	undistortor.imageUndistort();
	templ = undistortor.getUndistImg();
	cv::Mat diffImg,undistImg1, undistImg2,undistImg3;
	cv::imshow("templ before ", templ);
	cv::imshow("img before ", img);
	cv::undistort(img, undistImg1, cameraMatrixMat, distCoeffsMat);
	cv::undistort(templ, undistImg2, cameraMatrixMat, distCoeffsMat);
	cv::imshow("undist img1  ", undistImg1);
	cv::imshow("unsidt img2  ", undistImg2);
	cv::absdiff(img, undistImg1,diffImg);
	cv::imshow("Diff img1", diffImg);
	cv::absdiff(templ, undistImg2, diffImg);
	cv::imshow("Diff img2", diffImg);
	img = undistImg1.clone();
	templ = undistImg2.clone();
	cv::undistort(img2, undistImg3, cameraMatrixMat, distCoeffsMat);
	img2 = undistImg3.clone();
	correspondPoints correspondingPointsVec;
	cv::Size rectSize = cv::Size(30, 30);
	Eigen::Matrix4f resultMatNew;
	findRotAndTransMatrixesWithTemplateMatch(img, templ, rectSize, resultMatNew);
	
	LocalTemplateMatching(img, templ, rectSize, correspondingPointsVec);
	
	cv::imshow("img1", img);
	cv::imshow("Img2", templ);

	cv::Mat imgTempClone = templ.clone();
	for (int i = 0; i < correspondingPointsVec.size(); ++i) {
		cv::circle(img, cv::Point(correspondingPointsVec.at(i).first.x + 30, correspondingPointsVec.at(i).first.y + 30), 2, cv::Scalar(0, 255, 0), 2);
		cv::circle(templ, cv::Point(correspondingPointsVec.at(i).second.x + 30, correspondingPointsVec.at(i).second.y + 30), 2, cv::Scalar(0, 255, 0), 2);
	}

	
	cv::imshow("result img1", img);
	cv::imshow("result img2", templ);
	
	cv::waitKey(0);
	
	//templ = imread(argv[2], 1);

	cv::Rect cropRect = cv::Rect(20, 20, img.cols - 50, img.rows - 50);
	cv::Mat templ1 = templ.clone();
	templ = templ(cropRect);
	
	cv::imshow("Templ image", templ);
	cv::imshow("Source image", img);
	cv::waitKey(0);

	/// Create Trackbar
	//char* trackbar_label = "Method: \n 0: SQDIFF \n 1: SQDIFF NORMED \n 2: TM CCORR \n 3: TM CCORR NORMED \n 4: TM COEFF \n 5: TM COEFF NORMED";
	//createTrackbar(trackbar_label, image_window, &match_method, max_Trackbar, MatchingMethod);
	cv::Rect resultRect;
	MatchingMethod(img, templ,resultRect);
//	cv::rectangle(img, resultRect, cv::Scalar(0, 255, 0), 1);
	std::cout << "Finded rect parameters " << resultRect.x << "  " << resultRect.y << std::endl;
	cv::imshow("Result finded rect", img);
	cv::waitKey(0);
	
	feauturePointsPairs featPointsRes;
	featPointsRes = getFeauturePoints(img, templ1);
	std::cout << "featPointsRes size " << std::endl;
	std::cout<< featPointsRes.first.size() << std::endl;
	for (int i = 0; i < featPointsRes.first.size();++i) {
	    cv::circle(templ1,featPointsRes.first.at(i),1,cv::Scalar(0,0,255),1);
		cv::circle(img, featPointsRes.second.at(i), 1, cv::Scalar(0, 0, 255), 1);
	//	cv::imshow("current img", img);
	//	cv::waitKey(0);
	}
	cv::imshow("Result Feature points1", templ1);
	cv::imshow("Result Feature points2", img);
	cv::waitKey(0);

	Eigen::Matrix4f resultMatRotAndTr;
	findRotAndTransMatrixes("D:\\upWorkProjs\\StomatchVideos\\testImages1\\punkteImage5.png", "D:\\upWorkProjs\\StomatchVideos\\testImages1\\punkteImage10.png", resultMatRotAndTr);
	
	std::string img1Path;// = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage10.png";
	std::string img2Path;// = "C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage15.png";
	

	

	//findRotAndTransMatrixes(img1Path, img2Path, resultMat);
	// Load data
	Eigen::Matrix4f resultMat;
	std::vector<Eigen::Matrix4f> rotAndTrVec;
	std::vector<PCD, Eigen::aligned_allocator<PCD> > data;
	
	if (argc != 4) {
		std::cout << "Arguments count are not correct, give 3 arguments" << std::endl;
		return 0;
	}

	
	loadData(argc, argv, data);

	// Check user input
	if (data.empty())
	{
		PCL_ERROR("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
		PCL_ERROR("[*] - multiple files can be added. The registration results of (i, i+1) will be registered against (i+2), etc");
		return (-1);
	}
	PCL_INFO("Loaded %d datasets.", (int)data.size());

	// Create a PCLVisualizer object
	p = new pcl::visualization::PCLVisualizer(argc, argv, "Pairwise Incremental Registration example");
	p->createViewPort(0.0, 0, 0.5, 1.0, vp_1);
	p->createViewPort(0.5, 0, 1.0, 1.0, vp_2);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr result(new pcl::PointCloud<pcl::PointXYZRGB>), source, target;
	//PointCloud::Ptr result(new PointCloud), source, target;
	Eigen::Matrix4f GlobalTransform = Eigen::Matrix4f::Identity(), pairTransform;


	int countOfFilesInDir = (int)argv[argc - 1];
	std::string dirPath = std::string(argv[1]);
	std::string fileMainPart = std::string(argv[2]);


	feauturePointsPairs featurePointsResTmp;
	

	for (size_t i = 1; i < data.size(); ++i)
	{
		img1Path = dirPath + "\\" + fileMainPart  + std::to_string((long int)(i * 5 )) + ".png";
		img2Path = dirPath + "\\" + fileMainPart  + std::to_string((long int)((i+1)* 5 )) + ".png";

		if (i == 1) {
			result = data[i-1].cloud;
		}
		cv::Mat img1Tmp = cv::imread(img1Path);
		cv::Mat img2Tmp = cv::imread(img2Path);
		findRotAndTransMatrixesWithTemplateMatch(img1Tmp, img2Tmp, cv::Size(30, 30), resultMat);
	//	findRotAndTransMatrixes(img1Path, img2Path, resultMat);
		rotAndTrVec.push_back(resultMat);
			source = data[i - 1].cloud;
			target = data[i].cloud;

			
		
		
		
		// Add visualization data
	//	showCloudsLeft(source, target);

	//	pcl::transformPointCloud(*source, *target, resultMat);

	

		PointCloud::Ptr temp(new PointCloud);
	//	PCL_INFO("Aligning %s (%d) with %s (%d).\n", data[i - 1].f_name.c_str(), source->points.size(), data[i].f_name.c_str(), target->points.size());
	//	pairAlign(source, target, temp, pairTransform, resultMat,true);
		
		
		
	
		//transform current pair into the global transform
	//	pcl::transformPointCloud(*temp, *result, GlobalTransform);
		GlobalTransform = GlobalTransform * pairTransform;
		std::stringstream ss;
		ss << i << ".pcd";
		//target = *source + *target;
		
		//pcl::transformPointCloud(*source, *target, resultMat);
		
		Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
		Eigen::Matrix4f transform_1 = Eigen::Matrix4f::Identity();
		for (int h = rotAndTrVec.size()-1; h >= 0; --h) {
			transform_1(0, 0) = rotAndTrVec.at(h)(0, 0);
			transform_1(0, 1) = rotAndTrVec.at(h)(0, 1);
			transform_1(0, 2) = rotAndTrVec.at(h)(0, 2);
			transform_1(1, 0) = rotAndTrVec.at(h)(1, 0);
			transform_1(1, 1) = rotAndTrVec.at(h)(1, 1);
			transform_1(1, 2) = rotAndTrVec.at(h)(1, 2);
			transform_1(2, 0) = rotAndTrVec.at(h)(2, 0);
			transform_1(2, 1) = rotAndTrVec.at(h)(2, 1);
			transform_1(2, 2) = rotAndTrVec.at(h)(2, 2);
			transform_2.translation() << resultMat(0, 3), resultMat(1, 3), 0; resultMat(2, 3);
			pcl::transformPointCloud(*target, *target, transform_2);
			pcl::transformPointCloud(*target, *target, transform_1);
			*result = *target + *result;

		}
		//    (row, column)

		// Define a translation of 2.5 meters on the x axis.
		//transform_1(0, 3) = 2.5;


		
		
	//	pcl::transformPointCloud(*result, *target, transform_2);
	//	pcl::transformPointCloud(*result, *result, transform_1);
		
		pcl::io::savePCDFile(ss.str(), *result, true);

		//update the global transform
		
		
		//save aligned pair, transformed into the first cloud's frame
		//pcl::io::savePCDFile(ss.str(), *result, true);

	}
}*/
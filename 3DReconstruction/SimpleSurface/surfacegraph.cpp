/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd
** All rights reserved.
** For any questions to The Qt Company, please use contact form at http://qt.io
**
** This file is part of the Qt Data Visualization module.
**
** Licensees holding valid commercial license for Qt may use this file in
** accordance with the Qt License Agreement provided with the Software
** or, alternatively, in accordance with the terms contained in a written
** agreement between you and The Qt Company.
**
** If you have questions regarding the use of this file, please use
** contact form at http://qt.io
**
****************************************************************************/
#include <iostream>

#include <algorithm>

#include "surfacegraph.h"

#include <QtDataVisualization/QValue3DAxis>
#include <QtDataVisualization/Q3DTheme>
#include <QtGui/QImage>
#include <QtCore/qmath.h>
#include <QtWidgets/QApplication>



#include <pcl/visualization/pcl_visualizer.h>


using namespace QtDataVisualization;

const int sampleCountX = 50;
const int sampleCountZ = 50;
const int heightMapGridStepX = 6;
const int heightMapGridStepZ = 6;
const float sampleMin = -8.0f;
const float sampleMax = 8.0f;

SurfaceGraph::SurfaceGraph(Q3DSurface *surface)
: m_graph(surface)
{
	m_graph->setAxisX(new QValue3DAxis);
	m_graph->setAxisY(new QValue3DAxis);
	m_graph->setAxisZ(new QValue3DAxis);

	m_sqrtSinProxy = new QSurfaceDataProxy();
	m_sqrtSinSeries = new QSurface3DSeries(m_sqrtSinProxy);
	fillSqrtSinProxy();
	std::string currentImagePath;
	cv::Mat inputImg;
//	for (int i = 1; i < 100; ++i){
		
		//inputImg = cv::imread("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage140.png");
	//inputImg = cv::imread("C:\\Users\\Artashes\\Documents\\Visual Studio 2013\\Projects\\SimpleSurface\\SimpleSurface\\inputImg.png");
//	cv::Mat inputImg7 = cv::imread("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage200.png");
	cv::Mat inputImg7 = cv::imread("C:\\Users\\Artashes\\Documents\\Visual Studio 2013\\Projects\\SimpleSurface\\SimpleSurface\\imageStitched.png");
//	cv::Mat inputImg7 = cv::imread("D:\\upWorkProjs\\inpainter_\\MSInpainterUITest\\imageRefFilt.png");
	//	cv::Mat inputImg7 = cv::imread("C:\\Users\\Artashes\\Desktop\\Stereo_Vision_Project\\ShapeFormShading_Matlab_CodeOld\\photos\\1510221933571151767175.jpg");
//	cv::Mat inputImg7 = cv::imread("C:\\Users\\Artashes\\Desktop\\Stereo_Vision_Project\\ShapeFormShading_Matlab_CodeOld\\photos\\1510221933571151767175.jpg.normal.png");
	cv::resize(inputImg7, inputImg7, cv::Size(250, 250));
	inputImg = inputImg7.clone();
	//}

//	cv::imshow("Original Image", inputImg);
	cv::Rect cropedRect = cv::Rect(10, 30, inputImg.cols - 60, inputImg.rows - 60);

   cv::Mat inputImg1 = inputImg.clone();
 
   m_currentImage = inputImg1.clone();
   inputImg = inputImg1.clone();
//	cv::imshow("Croped Image", inputImg);
	
//	cv::cvtColor(inputImg, inputImg, CV_BGR2GRAY);
//	cv::equalizeHist(inputImg, inputImg);
   std::vector<cv::Mat> channelsOfImg;
   cv::cvtColor(inputImg, inputImg, CV_BGR2YCrCb);
   cv::split(inputImg, channelsOfImg);
   cv::equalizeHist(channelsOfImg[0], channelsOfImg[0]);
   cv::merge(channelsOfImg, inputImg);
	cv::cvtColor(inputImg, inputImg, CV_YCrCb2BGR);
	//cv::blur(inputImg, inputImg, cv::Size(11, 11));
	//cv::GaussianBlur(inputImg, inputImg,cv::Size(9,9),2);
	//cv::cvtColor(inputImg, inputImg, CV_BGR2GRAY);
//	cv::imshow("Image", inputImg);
//	cv::waitKey(0);
	QImage heightMapImage((uchar*)inputImg.data, inputImg.cols, inputImg.rows,inputImg.step, QImage::Format_RGB888);
	
//	QImage heightMapImage("C:/Users/Artashes/Desktop/StereoVsionSimple/StereoVsionSimple/StomachImages/left/leftStomachImage1.png");
//	QImage heightMapImage("C:/Users/Artashes/Documents/Visual Studio 2010/Projects/StereoDepthMapTuning/StereoDepthMapTuning/FilteredLeft.jpg");
	m_heightMapProxy = new QHeightMapSurfaceDataProxy(heightMapImage);
	m_heightMapSeries = new QSurface3DSeries(m_heightMapProxy);
	m_heightMapSeries->setItemLabelFormat(QStringLiteral("(@xLabel, @zLabel): @yLabel"));
	
















	QImage heightMapImage1((uchar*)inputImg7.data, inputImg7.cols, inputImg7.rows, inputImg7.step, QImage::Format_RGB888);;// = m_heightMapProxy->heightMap();
	
	
	int depthInfo = m_heightMapProxy->heightMap().depth();
	std::cout << "DEpth Info  " << depthInfo << std::endl;
	cv::Mat heightMapMat(heightMapImage1.height(), heightMapImage1.width(), CV_8UC3, (uchar*)heightMapImage1.bits(), heightMapImage1.bytesPerLine());
	cv::cvtColor(heightMapMat, heightMapMat, CV_BGR2RGB);
//	cv::imshow("Height Map", heightMapMat);
//	cv::waitKey(0);
	m_heightMapProxy->setValueRanges(34.0f, 40.0f, 18.0f, 24.0f);

	QApplication::processEvents();
	std::cout << "Height Map series init7777 " << m_heightMapSeries->dataProxy()->columnCount() << std::endl;
//	m_heightMapProxy->resetArray(;
//	m_heightMapProxy->setHeightMap(heightMapImage1);
//	m_heightMapSeries->dataProxy()->arrayReset();
//	m_heightMapSeries->dataProxyChanged(m_heightMapProxy);
		//= new QSurface3DSeries(m_heightMapProxy);
	QApplication::processEvents();

	std::cout << "Height Map series init8888 " << m_heightMapSeries->dataProxy()->columnCount() << std::endl;
	m_heightMapWidth = heightMapImage.width();
	m_heightMapHeight = heightMapImage.height();
	//writeDataToPCDFile(m_heightMapSeries, "newPcdTest.pcd");
}

SurfaceGraph::~SurfaceGraph()
{
	delete m_graph;
}

void SurfaceGraph::fillSqrtSinProxy()
{
	float stepX = (sampleMax - sampleMin) / float(sampleCountX - 1);
	float stepZ = (sampleMax - sampleMin) / float(sampleCountZ - 1);

	QSurfaceDataArray *dataArray = new QSurfaceDataArray;
	dataArray->reserve(sampleCountZ);
	for (int i = 0; i < sampleCountZ; i++) {
		QSurfaceDataRow *newRow = new QSurfaceDataRow(sampleCountX);
		// Keep values within range bounds, since just adding step can cause minor drift due
		// to the rounding errors.
		float z = qMin(sampleMax, (i * stepZ + sampleMin));
		int index = 0;
		for (int j = 0; j < sampleCountX; j++) {
			float x = qMin(sampleMax, (j * stepX + sampleMin));
			float R = qSqrt(z * z + x * x) + 0.01f;
			float y = (qSin(R) / R + 0.24f) * 1.61f;
			(*newRow)[index++].setPosition(QVector3D(x, y, z));
			
		}
		*dataArray << newRow;
	}

	m_sqrtSinProxy->resetArray(dataArray);
	

}

void SurfaceGraph::createImageFromHeightMap(cv::Mat& resImg)
{
	int rowsOfImg = m_sqrtSinProxy->rowCount();
	int colsOfImg = m_sqrtSinProxy->columnCount();
	resImg = cv::Mat(rowsOfImg, colsOfImg, CV_8UC1);
	/* height map for (int i = 0; i < rowsOfImg; ++i) {
		for (int j = 0; j < colsOfImg; ++j) {
		
		//	std::cout << "Current Value Of Z " <<  <<" "<< std::endl;
		}
	}*/
}


void SurfaceGraph::enableSqrtSinModel(bool enable)
{
	if (enable) {
		m_sqrtSinSeries->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
		m_sqrtSinSeries->setFlatShadingEnabled(true);

		m_graph->axisX()->setLabelFormat("%.2f");
		m_graph->axisZ()->setLabelFormat("%.2f");
		m_graph->axisX()->setRange(sampleMin, sampleMax);
		m_graph->axisY()->setRange(0.0f, 2.0f);
		m_graph->axisZ()->setRange(sampleMin, sampleMax);
		m_graph->axisX()->setLabelAutoRotation(30);
		m_graph->axisY()->setLabelAutoRotation(90);
		m_graph->axisZ()->setLabelAutoRotation(30);

		m_graph->removeSeries(m_heightMapSeries);
		m_graph->addSeries(m_sqrtSinSeries);

		// Reset range sliders for Sqrt&Sin
		m_rangeMinX = sampleMin;
		m_rangeMinZ = sampleMin;
		m_stepX = (sampleMax - sampleMin) / float(sampleCountX - 1);
		m_stepZ = (sampleMax - sampleMin) / float(sampleCountZ - 1);
		m_axisMinSliderX->setMaximum(sampleCountX - 2);
		m_axisMinSliderX->setValue(0);
		m_axisMaxSliderX->setMaximum(sampleCountX - 1);
		m_axisMaxSliderX->setValue(sampleCountX - 1);
		m_axisMinSliderZ->setMaximum(sampleCountZ - 2);
		m_axisMinSliderZ->setValue(0);
		m_axisMaxSliderZ->setMaximum(sampleCountZ - 1);
		m_axisMaxSliderZ->setValue(sampleCountZ - 1);
	}
}

void SurfaceGraph::writeDataToPCDFile( QSurface3DSeries * heightMapSeries, const std::string& filePath)
{
	cv::Mat inputImg1 = cv::imread("C:\\Users\\Artashes\\Desktop\\StereoVsionSimple\\StereoVsionSimple\\StomachImages\\left\\leftStomachImage140.png");
	cv::Rect cropedRect = cv::Rect(10, 30, inputImg1.cols - 60, inputImg1.rows - 60);
	cv::Mat inputImg = inputImg1(cropedRect);
	cv::Mat depthMapImage = cv::imread("depthMap.png");
	cv::Mat inputImgRot = inputImg.clone();

/*	QImage heightMapImage((uchar*)inputImg.data, inputImg.cols, inputImg.rows, inputImg.step, QImage::Format_RGB888);
	
	m_heightMapProxy = new QHeightMapSurfaceDataProxy(heightMapImage);
	m_heightMapSeries = new  QSurface3DSeries(m_heightMapProxy);*/
	//m_heightMapSeries

	double angle = -90;  // or 270
	cv::Size src_sz = inputImgRot.size();
	cv::Size dst_sz(src_sz.height, src_sz.width);

	int len = std::max(inputImgRot.cols, inputImgRot.rows);
	cv::Point2f center(len / 2., len / 2.);
	cv::Mat rot_mat = cv::getRotationMatrix2D(center, angle, 1.0);
	cv::warpAffine(inputImgRot, inputImgRot, rot_mat, dst_sz);
	cv::warpAffine(depthMapImage, depthMapImage, rot_mat, dst_sz);

	cv::Mat imgRotTr = inputImgRot.clone();
	for (int i = 0; i < imgRotTr.rows/2; ++i) {
		for (int j = 0; j < imgRotTr.cols; ++j) {
			imgRotTr.at<cv::Vec3b>(i, j) = inputImg.at<cv::Vec3b>(imgRotTr.rows - 1 - i, j);
			imgRotTr.at<cv::Vec3b>(imgRotTr.rows - 1 - i, j) = inputImg.at<cv::Vec3b>(i, j);
		}
	}
	cv::imwrite("rotImg.png", imgRotTr);
	pcl::PointCloud<pcl::PointXYZRGB> cloud;
	cloud.width = heightMapSeries->dataProxy()->columnCount() * heightMapSeries->dataProxy()->rowCount();
	cloud.height = 1;
	cloud.is_dense = false;
	cloud.points.resize(cloud.width * cloud.height);
	for (int i = 0; i < m_heightMapSeries->dataProxy()->rowCount(); ++i) {
		QSurfaceDataRow* tmpRow = m_heightMapSeries->dataProxy()->array()->at(i);
		for (int j = 0; j < tmpRow->size(); ++j) {
			cloud.points[i*m_heightMapWidth + j].x = j;
			cloud.points[i*m_heightMapWidth + j].y =  tmpRow->at(j).y();
			cloud.points[i* m_heightMapWidth + j].z = i;
			cloud.points[i*m_heightMapWidth + j].b =  imgRotTr.at<cv::Vec3b>(i, j)[0];
			cloud.points[i*m_heightMapWidth + j].g =  imgRotTr.at<cv::Vec3b>(i, j)[1];
			cloud.points[i*m_heightMapWidth + j].r =  imgRotTr.at<cv::Vec3b>(i, j)[2];
			float tmpPixelVal = tmpRow->at(j).y();

		}

	}

	/*
	cv::Mat depthImage = cv::imread("depthMap.png");


	if (depthImage.channels() == 3){
		cv::cvtColor(depthImage, depthImage, CV_BGR2GRAY);
	}
	*/
/*	for (size_t i = 0; i < cloud.points.size(); ++i)
	{
		int tmpIndexRow = i / heightMapSeries->dataProxy()->columnCount();
		int tmpIndexCol = i % heightMapSeries->dataProxy()->columnCount();
		cloud.points[i].x = tmpIndexCol;
		cloud.points[i].y = depthMapImage.at<uchar>(tmpIndexRow, tmpIndexCol);
		cloud.points[i].z = tmpIndexRow;
		cloud.points[i].b = inputImg.at<cv::Vec3b>(tmpIndexRow, tmpIndexCol)[0];
		cloud.points[i].g = inputImg.at<cv::Vec3b>(tmpIndexRow, tmpIndexCol)[1];
		cloud.points[i].r = inputImg.at<cv::Vec3b>(tmpIndexRow, tmpIndexCol)[2];
	//	cloud.points[i].x = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).x();
//		cloud.points[i].y = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).z();
//		cloud.points[i].z = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).y();
	//	cloud.points[i].x = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).x();
	//	cloud.points[i].y = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).y();
	//	cloud.points[i].z = heightMapSeries->dataProxy()->array()->at(tmpIndexRow)->at(tmpIndexCol).z();
	}
	*/
	//pcl::io::savePCDFileASCII(filePath, cloud);
	pcl::io::savePCDFileASCII(filePath, cloud);
	std::cerr << "Saved " << cloud.points.size() << " data points to" << filePath << "  file " << std::endl;
}
void SurfaceGraph::readAndShowPointCloud(const std::string& pointCloudFilePath)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

	if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pointCloudFilePath, *cloud) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file  .pcd \n");
		return ;
	}
	std::cout << "Loaded "
		<< cloud->width * cloud->height
		<< " data points from test_pcd.pcd with the following fields: "
		<< std::endl;


	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloud_in_color_h3(cloud, (int)0, (int)0,

		(int)255);

	pcl::visualization::PCLVisualizer viewer("ICP DEMO");
	int v1(0);
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.addPointCloud(cloud,/* cloud_in_color_h3,*/ "cloud in color", v1);
	while (!viewer.wasStopped())
	{
		viewer.spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}

}

void SurfaceGraph::enableHeightMapModel(bool enable)
{
	if (enable) {
		std::cout << "Height Map series init9999 " << m_heightMapSeries->dataProxy()->columnCount() << std::endl;
		cv::Mat tmpDepthMap = cv::Mat(m_heightMapHeight, m_heightMapWidth, CV_8UC1);

		for (int i = 0; i < m_heightMapHeight; ++i) {
			QSurfaceDataRow* tmpRow = m_heightMapSeries->dataProxy()->array()->at(i);
			for (int j = 0; j < m_heightMapWidth; ++j) {
				//QSurfaceDataRow* tmpRow = m_heightMapSeries->dataProxy()->array()->at(i);
				float tmpPixelVal = tmpRow->at(j).y();
				uchar tmpPixelValU;
				if (tmpPixelVal > 255) {
					tmpPixelValU = 255;
				}
				else {
					tmpPixelValU = (uchar)(tmpPixelVal);
				}
				tmpDepthMap.at<uchar>(j, i) = tmpPixelValU;
			}

		}
	

		double angle = 90;  // or 270
		cv::Size src_sz = tmpDepthMap.size();
		cv::Size dst_sz(src_sz.height, src_sz.width);

		int len = std::max(tmpDepthMap.cols, tmpDepthMap.rows);
		cv::Point2f center(len / 2., len / 2.);
		cv::Mat rot_mat = cv::getRotationMatrix2D(center, angle, 1.0);
		cv::warpAffine(tmpDepthMap, tmpDepthMap, rot_mat, dst_sz);


		cv::Mat newDepthMap = m_currentImage.clone();
		cv::cvtColor(newDepthMap, newDepthMap, CV_BGR2GRAY);
		cv::equalizeHist(newDepthMap, newDepthMap);
	
		
		cv::imwrite("depthMap.png",tmpDepthMap);
	
		std::cout << "Height Map series init  New" << m_heightMapSeries->dataProxy()->columnCount() << std::endl;
	//	writeDataToPCDFile(m_heightMapSeries, "heightMapPointCloud.pcd");
	//	readAndShowPointCloud("heightMapPointCloud.pcd");
		m_heightMapSeries->setDrawMode(QSurface3DSeries::DrawSurface);
	//	m_heightMapSeries->setBaseColor(QColor(255, 255, 255));
		//m_heightMapSeries->setDrawMode(QSurface3DSerie);
	//	m_heightMapSeries->setSingleHighlightColor(QColor(125, 125, 125));
		m_heightMapSeries->setFlatShadingEnabled(false);


		m_graph->axisX()->setLabelFormat("%.1f N");
		m_graph->axisZ()->setLabelFormat("%.1f E");
		m_graph->axisX()->setRange(34.0f, 40.0f);
		m_graph->axisY()->setAutoAdjustRange(true);
		m_graph->axisZ()->setRange(18.0f, 24.0f);

		m_graph->axisX()->setTitle(QStringLiteral("Latitude"));
		m_graph->axisY()->setTitle(QStringLiteral("Height"));
		m_graph->axisZ()->setTitle(QStringLiteral("Longitude"));
		
		m_graph->removeSeries(m_sqrtSinSeries);
		m_graph->addSeries(m_heightMapSeries);
		
		//QSurfaceDataArray* tmpArray = new QSurfaceDataArray;
		const QSurfaceDataArray*  tmpArray = m_heightMapSeries->dataProxy()->array();
	    

	//	cv::imshow("Depth Map ", tmpDepthMap);
	//	cv::waitKey(0);

		 //std::cout << "Values OF tmp ROW " << std::endl;
		/* for (int i = 0; i < tmpRow->size(); ++i) {
			 std::cout << "Value " << tmpRow->at(i).y() << std::endl;
		 }*/
		// std::cout << "Values OF tmp ROW END " << std::endl;

	//	tmpList.ro
		//QSurfaceDataProxy* tmpDataProxy = m_heightMapSeries->dataProxy();
		QSurfaceDataArray* tmpArrayNew;
		
/*		float y1 = tmpArray->at(0)->at(0).z();
		for (int i = 0; i < m_heightMapHeight; ++i) {
			for (int j = 0; j < m_heightMapWidth; ++j) {
				//std::cout << "Z of current pixel" << m_heightMapSeries->dataProxy()->array()->at(i)->at(j).z()<< std::endl;
			//	m_heightMapSeries->dataProxy()->array()->at(i)->at(j).z();
			}
		}*/
		std::cout << "Height Map width " << m_heightMapWidth << "   " << "Height Map height " << m_heightMapHeight << std::endl;
		// Reset range sliders for height map
		int mapGridCountX = m_heightMapWidth / heightMapGridStepX;
		int mapGridCountZ = m_heightMapHeight / heightMapGridStepZ;
		m_rangeMinX = 34.0f;
		m_rangeMinZ = 18.0f;
		m_stepX = 6.0f / float(mapGridCountX - 1);
		m_stepZ = 6.0f / float(mapGridCountZ - 1);
		m_axisMinSliderX->setMaximum(mapGridCountX - 2);
		m_axisMinSliderX->setValue(0);
		m_axisMaxSliderX->setMaximum(mapGridCountX - 1);
		m_axisMaxSliderX->setValue(mapGridCountX - 1);
		m_axisMinSliderZ->setMaximum(mapGridCountZ - 2);
		m_axisMinSliderZ->setValue(0);
		m_axisMaxSliderZ->setMaximum(mapGridCountZ - 1);
		m_axisMaxSliderZ->setValue(mapGridCountZ - 1);
	}
}

void SurfaceGraph::adjustXMin(int min)
{
	float minX = m_stepX * float(min) + m_rangeMinX;

	int max = m_axisMaxSliderX->value();
	if (min >= max) {
		max = min + 1;
		m_axisMaxSliderX->setValue(max);
	}
	float maxX = m_stepX * max + m_rangeMinX;

	setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustXMax(int max)
{
	float maxX = m_stepX * float(max) + m_rangeMinX;

	int min = m_axisMinSliderX->value();
	if (max <= min) {
		min = max - 1;
		m_axisMinSliderX->setValue(min);
	}
	float minX = m_stepX * min + m_rangeMinX;

	setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustZMin(int min)
{
	float minZ = m_stepZ * float(min) + m_rangeMinZ;

	int max = m_axisMaxSliderZ->value();
	if (min >= max) {
		max = min + 1;
		m_axisMaxSliderZ->setValue(max);
	}
	float maxZ = m_stepZ * max + m_rangeMinZ;

	setAxisZRange(minZ, maxZ);
}

void SurfaceGraph::adjustZMax(int max)
{
	float maxX = m_stepZ * float(max) + m_rangeMinZ;

	int min = m_axisMinSliderZ->value();
	if (max <= min) {
		min = max - 1;
		m_axisMinSliderZ->setValue(min);
	}
	float minX = m_stepZ * min + m_rangeMinZ;

	setAxisZRange(minX, maxX);
}

void SurfaceGraph::setAxisXRange(float min, float max)
{
	m_graph->axisX()->setRange(min, max);
}

void SurfaceGraph::setAxisZRange(float min, float max)
{
	 m_graph->axisZ()->setRange(min, max);
}

void SurfaceGraph::changeTheme(int theme)
{
	m_graph->activeTheme()->setType(Q3DTheme::Theme(theme));
}

void SurfaceGraph::setBlackToYellowGradient()
{
	QLinearGradient gr;
	
	gr.setColorAt(0.0, Qt::black);
	gr.setColorAt(0.33, Qt::blue);
	gr.setColorAt(0.67, Qt::red);
	gr.setColorAt(1.0, Qt::yellow);
	
	m_graph->seriesList().at(0)->setBaseGradient(gr);
	m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);

}



void SurfaceGraph::setGreenToRedGradient()
{
	QLinearGradient gr(QPointF(240,240),QPointF(250,250));
	

//	QRadialGradient gr;
	/*gr.setColorAt(0.0, Qt::red);
	gr.setColorAt(0.5, Qt::yellow);
	gr.setColorAt(0.8, Qt::red);
	gr.setColorAt(1.0, Qt::darkRed);*/

	//gr.setStart(QPointF(100,100));
	//gr.setColorAt(0.0, Qt::red);
    

	QRadialGradient radGra;

//	cv::imshow("Curerent IMage", m_currentImage);
//	cv::waitKey(0);
	std::vector<int> pixeliDList;
	std::vector<int> pixelList1;
	std::vector<int> pixelList2;
	std::vector<int> pixelList3;
	for (int i = 0; i < m_currentImage.cols; i += 1) {
		cv::Vec3b tmpPixel = m_currentImage.at<cv::Vec3b>(m_currentImage.rows / 2, i);
	//	uchar tmpPixelUchar = m_currentImage.at<cv::Vec3b>(m_currentImage.rows / 2, i)[0];
	//	float  tmpVal = (float)((float)i / (float)m_currentImage.cols);
	//	std::cout << "Current cols  " << tmpVal << std::endl;
	//	std::cout << "Pixel Value  " <<(int) tmpPixel[0] << "  " <<(int) tmpPixel[1] << "  " <<(int) tmpPixel[2] <<"Uchar  "<<(int)tmpPixelUchar<< std::endl;
	//	QColor tmpColor = QColor((int)tmpPixel[2], (int)tmpPixel[1], (int)tmpPixel[0]);

	//	pixeliDList.push_back(i);
		pixelList1.push_back((int)tmpPixel[0]);
		pixelList2.push_back((int)tmpPixel[1]);
		pixelList3.push_back((int)tmpPixel[2]);

	}
	std::sort(pixelList1.begin(), pixelList1.end());
	std::sort(pixelList2.begin(), pixelList2.end());
	std::sort(pixelList3.begin(), pixelList3.end());

	for (int i = 0; i < pixelList1.size(); ++i) {
		float tmpVal = (float)((float)i / (float)pixelList1.size());
		QColor tmpColor = QColor(pixelList3.at(i), pixelList2.at(i), pixelList1.at(i));
		gr.setColorAt(tmpVal, tmpColor);
	}
//	 
	//gr.setColorAt(1.0, QColor(240, 240, 240));
	/*gr.setColorAt(0.0, QColor(36, 36, 32));
	gr.setColorAt(0.3, QColor(50, 50, 46));
	gr.setColorAt(0.5, QColor(106, 104, 90));
	gr.setColorAt(0.8, QColor(150, 150, 120));
	gr.setColorAt(1.0, QColor(212, 235, 211));
	gr.setColorAt(1.2, QColor(230, 230, 230));*/
	
//	m_graph->seriesList().at(0)->setBaseGradient(gr);
	//m_graph->seriesList().at(0)->
	
	
	m_graph->seriesList().at(0)->setBaseGradient(gr);
	m_graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);
	
	//m_graph->seriesList().at(0)->setBaseColor(QColor(212, 235, 211));
	
}
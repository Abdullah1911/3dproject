/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd
** All rights reserved.
** For any questions to The Qt Company, please use contact form at http://qt.io
**
** This file is part of the Qt Data Visualization module.
**
** Licensees holding valid commercial license for Qt may use this file in
** accordance with the Qt License Agreement provided with the Software
** or, alternatively, in accordance with the terms contained in a written
** agreement between you and The Qt Company.
**
** If you have questions regarding the use of this file, please use
** contact form at http://qt.io
**
****************************************************************************/

#ifndef SURFACEGRAPH_H
#define SURFACEGRAPH_H

#include <QtDataVisualization/Q3DSurface>

#include <QtDataVisualization/QSurfaceDataProxy>
#include <QtDataVisualization/QHeightMapSurfaceDataProxy>
#include <QtDataVisualization/QSurface3DSeries>
#include <QtWidgets/QSlider>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>


using namespace QtDataVisualization;

class SurfaceGraph : public QObject
{
//	Q_OBJECT
public:
	explicit SurfaceGraph(Q3DSurface *surface);
	~SurfaceGraph();

	void enableHeightMapModel(bool enable);
	void enableSqrtSinModel(bool enable);
	void writeDataToPCDFile(QSurface3DSeries * heightMapSeries, const std::string& filePath);
	void readAndShowPointCloud(const std::string& pointCloudFilePath);
	void toggleModeNone() { m_graph->setSelectionMode(QAbstract3DGraph::SelectionNone); }
	void toggleModeItem() { m_graph->setSelectionMode(QAbstract3DGraph::SelectionItem); }
	void toggleModeSliceRow() {
		m_graph->setSelectionMode(QAbstract3DGraph::SelectionItemAndRow
			| QAbstract3DGraph::SelectionSlice);
	}
	void toggleModeSliceColumn() {
		m_graph->setSelectionMode(QAbstract3DGraph::SelectionItemAndColumn
			| QAbstract3DGraph::SelectionSlice);
	}

	void setBlackToYellowGradient();
	void setGreenToRedGradient();

	void setAxisMinSliderX(QSlider *slider) { m_axisMinSliderX = slider; }
	void setAxisMaxSliderX(QSlider *slider) { m_axisMaxSliderX = slider; }
	void setAxisMinSliderZ(QSlider *slider) { m_axisMinSliderZ = slider; }
	void setAxisMaxSliderZ(QSlider *slider) { m_axisMaxSliderZ = slider; }

	void adjustXMin(int min);
	void adjustXMax(int max);
	void adjustZMin(int min);
	void adjustZMax(int max);

	public slots:
	void changeTheme(int theme);

private:
	Q3DSurface *m_graph;
	QHeightMapSurfaceDataProxy *m_heightMapProxy;
	QSurfaceDataProxy *m_sqrtSinProxy;
	QSurface3DSeries *m_heightMapSeries;
	QSurface3DSeries *m_sqrtSinSeries;

	QSlider *m_axisMinSliderX;
	QSlider *m_axisMaxSliderX;
	QSlider *m_axisMinSliderZ;
	QSlider *m_axisMaxSliderZ;
	cv::Mat m_currentImage;
	float m_rangeMinX;
	float m_rangeMinZ;
	float m_stepX;
	float m_stepZ;
	int m_heightMapWidth;
	int m_heightMapHeight;

	void setAxisXRange(float min, float max);
	void setAxisZRange(float min, float max);
	void createImageFromHeightMap(cv::Mat& resImg);
	void fillSqrtSinProxy();
};

#endif // SURFACEGRAPH_H
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/stitching/stitcher.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <iostream>

#include "templateMatching.h"



void LocalTemplateMatching(const cv::Mat& sourceImg1, const cv::Mat& sourceImg2, const cv::Size& rectSize,  correspondPoints& correspondingPointsVec, std::vector<cv::Point2f>& pointsOpposite)
{
	/// Source image to display
	cv::Mat sourceImgCroped1;
	cv::Mat sourceImgCroped2;

	cv::Rect cropedRect = cv::Rect(30, 30, sourceImg1.cols - 60, sourceImg1.rows - 60);
	//cv::Rect cropedRect = cv::Rect(10, 10, sourceImg1.cols - 30, sourceImg1.rows - 30);
	sourceImgCroped1 = sourceImg1(cropedRect);
	sourceImgCroped2 = sourceImg2(cropedRect);
	cv::Mat tmpPixelRectMat1;
	cv::Mat tmpPixelRectMat2;
	cv::Rect tmpPixelRect1;
	cv::Rect tmpPixelRect2;
	cv::Point2f tmpCenterPixel1;
	cv::Point2f tmpCenterPixel2;
	int result_cols = sourceImgCroped2.cols - rectSize.width + 1;
	int result_rows = sourceImgCroped2.rows - rectSize.height + 1;
	cv::Mat result;
	result.create(result_rows, result_cols, CV_32FC1);
	for (int i = 1.5*rectSize.height; i < sourceImgCroped1.rows - 1.5*rectSize.height; i+=20 /*rectSize.width*/) {
		for (int j = 1.5*rectSize.width; j < sourceImgCroped1.cols - 1.5*rectSize.width; j+=20 /*rectSize.height*/) {
			tmpPixelRect1.y = i; 
			tmpPixelRect1.x = j;
			tmpPixelRect1.width = rectSize.width; 
			tmpPixelRect1.height = rectSize.height; 
			//std::cout << "TMp pixel rect " << tmpPixelRect1.x << "  " << tmpPixelRect1.y << "  " << tmpPixelRect1.height << "  " << tmpPixelRect1.width << std::endl;
			//cv::rectangle(sourceImgCroped1, tmpPixelRect1, cv::Scalar(0, 255, 0), 2);
			//cv::imshow("source image", sourceImgCroped1);
			//cv::waitKey(0);
			tmpPixelRectMat1 = sourceImgCroped1(tmpPixelRect1);
			tmpCenterPixel1.x = (float)((float)tmpPixelRect1.x + (float)rectSize.width / 2);
			tmpCenterPixel1.y =  (float)(float)tmpPixelRect1.y + (float)rectSize.height / 2;
			//cv::imshow("template img", tmpPixelRectMat1);
			
		//	cv::waitKey(0);
			cv::Mat sourceImgCropedSmallMat;
			cv::Rect sourceImg2CropeRect = cv::Rect(tmpPixelRect1.x - rectSize.width/2,tmpPixelRect1.y - rectSize.height/2,tmpPixelRect1.width + rectSize.width, tmpPixelRect1.height + rectSize.height);
		/*	cv::rectangle(sourceImgCroped2, sourceImg2CropeRect, cv::Scalar(255, 0, 0), 2);
			cv::rectangle(sourceImgCroped1, tmpPixelRect1, cv::Scalar(255, 0, 0), 2);
			cv::imshow("rect2 ", sourceImgCroped2);
			cv::imshow("rect1", sourceImgCroped1);
			cv::waitKey(0);*/
			sourceImgCropedSmallMat = sourceImgCroped2(sourceImg2CropeRect);
			cv::matchTemplate(sourceImgCropedSmallMat, tmpPixelRectMat1, result, CV_TM_CCORR_NORMED);
			normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());
			double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
			cv::Point matchLoc;
			cv::minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());
			matchLoc = maxLoc;
			matchLoc.x = matchLoc.x + tmpPixelRect1.x - rectSize.width / 2;
			matchLoc.y = matchLoc.y + tmpPixelRect1.y - rectSize.height / 2;
			tmpPixelRect2 =   cv::Rect(matchLoc, cv::Point(matchLoc.x + rectSize.width, matchLoc.y + rectSize.height));
	//		std::cout << "TMP RECT1 " << tmpPixelRect1.x << "  " << tmpPixelRect1.y << "  " << tmpPixelRect1.width << "  " << tmpPixelRect1.height << std::endl;
	//		std::cout << "TMP RECT2 " << tmpPixelRect2.x << "  " << tmpPixelRect2.y << "  " << tmpPixelRect2.width << "  " << tmpPixelRect2.height << std::endl;
			tmpPixelRectMat2 = sourceImgCroped2(tmpPixelRect2);
			tmpCenterPixel2.x = (float)((float)tmpPixelRect2.x + (float)rectSize.width / 2);
			tmpCenterPixel2.y = (float)((float)tmpPixelRect2.y + (float)rectSize.height / 2);
			std::pair<cv::Point2f, cv::Point2f> tmpPointsPair;
			tmpPointsPair.first = tmpCenterPixel1;
			tmpPointsPair.second = tmpCenterPixel2;
			correspondingPointsVec.push_back(tmpPointsPair);
			cv::Rect sourceImg1CropeRect = cv::Rect(tmpPixelRect2.x - rectSize.width / 2, tmpPixelRect2.y - rectSize.height / 2, tmpPixelRect2.width + rectSize.width, tmpPixelRect2.height + rectSize.height);
			sourceImgCropedSmallMat = sourceImgCroped1(sourceImg1CropeRect);
			cv::matchTemplate(tmpPixelRectMat2, sourceImgCropedSmallMat, result, CV_TM_CCORR_NORMED);
			normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());
			cv::minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());
			matchLoc = maxLoc;
			matchLoc.x = matchLoc.x + tmpPixelRect1.x - rectSize.width / 2;
			matchLoc.y = matchLoc.y + tmpPixelRect1.y - rectSize.height / 2;
			tmpPixelRect1 = cv::Rect(matchLoc, cv::Point(matchLoc.x + rectSize.width, matchLoc.y + rectSize.height));
			tmpPixelRectMat1 = sourceImgCroped1(tmpPixelRect1);
			tmpCenterPixel1.x = (float)((float)tmpPixelRect1.x + (float)rectSize.width / 2);
			tmpCenterPixel1.y = (float)((float)tmpPixelRect1.y + (float)rectSize.height / 2);
			pointsOpposite.push_back(tmpCenterPixel1);
			//break;
		}
		//break;
	}




}

void MatchingMethod(const cv::Mat& sourceImg, const cv::Mat& tempImg, cv::Rect& resultRect)
{
	/// Source image to display
	cv::Mat img_display;
	sourceImg.copyTo(img_display);

	/// Create the result matrix
	int result_cols = sourceImg.cols - tempImg.cols + 1;
	int result_rows = sourceImg.rows - tempImg.rows + 1;
	cv::Mat result;
	result.create(result_rows, result_cols, CV_32FC1);

	/// Do the Matching and Normalize
	cv::matchTemplate(sourceImg, tempImg, result, CV_TM_CCORR_NORMED);
	normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

	/// Localizing the best match with minMaxLoc
	double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
	cv::Point matchLoc;

	cv::minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());

	/// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better

    matchLoc = maxLoc;
	
	/// Show me what you got
	cv::rectangle(img_display, matchLoc, cv::Point(matchLoc.x + tempImg.cols, matchLoc.y + tempImg.rows), cv::Scalar::all(0), 2, 8, 0);
	rectangle(result, matchLoc, cv::Point(matchLoc.x + tempImg.cols, matchLoc.y + tempImg.rows), cv::Scalar::all(0), 2, 8, 0);
	resultRect.x = matchLoc.x;
	resultRect.y = matchLoc.y;
	resultRect.width = tempImg.cols;
	resultRect.height = tempImg.rows;

	cv::imshow("image_window", img_display);
	cv::imshow("result_window", result);
	cv::waitKey(0);
	return;
}

feauturePointsPairs getFeauturePoints(const cv::Mat& img1, const cv::Mat& img2)
{
	
	feauturePointsPairs feauturePointsRes;
	//cv::Rect cropedRect = cv::Rect(20, 20, img1.cols - 50, img1.rows - 50);
	cv::Rect cropedRect = cv::Rect(40, 40, 140, 140);
	cv::Mat tempImg = img2(cropedRect);
	cv::Rect rectMatching;
	int tmpX= 0;
	int tmpY = 0;
	MatchingMethod(img1, tempImg, rectMatching);
	for (int i = 0; i < cropedRect.width; ++i) {
		for (int j = 0; j < cropedRect.height; ++j) {
			feauturePointsRes.first.push_back(cv::Point(cropedRect.x + i,cropedRect.y + j));
			feauturePointsRes.second.push_back(cv::Point(rectMatching.x + i, rectMatching.y + j));
			
		}
		
	}
	/*feauturePointsRes.first.push_back(cv::Point(cropedRect.x , cropedRect.y ));
	feauturePointsRes.second.push_back(cv::Point(rectMatching.x , rectMatching.y ));
	feauturePointsRes.first.push_back(cv::Point(cropedRect.x + cropedRect.width, cropedRect.y));
	feauturePointsRes.second.push_back(cv::Point(rectMatching.x + cropedRect.width, rectMatching.y));
	feauturePointsRes.first.push_back(cv::Point(cropedRect.x + cropedRect.width, cropedRect.y + cropedRect.height));
	feauturePointsRes.second.push_back(cv::Point(rectMatching.x + cropedRect.width, rectMatching.y + cropedRect.height));
	feauturePointsRes.first.push_back(cv::Point(cropedRect.x, cropedRect.y + cropedRect.height));
	feauturePointsRes.second.push_back(cv::Point(rectMatching.x, rectMatching.y + cropedRect.height));
	*/
	
	std::cout << "End of function getFeauturePoints " << std::endl;
	return feauturePointsRes;
}

#include "csvParser.h"



void parseCSVFile(const QString& filePath, const QString& outputFilePath, const std::vector<std::vector<double>>& dataFrames)
{
	QFile fileCsv(filePath);
	if (!fileCsv.open(QIODevice::ReadWrite)){
		qDebug() << fileCsv.errorString();
		return;
	}
	QStringList wordList;
	while (!fileCsv.atEnd())
	{
		QByteArray line = fileCsv.readLine();
		wordList.append(line.split('\r\n').first());
	}

	QFile fileOut(outputFilePath);
	QString tmpString;
	for (int i = 0; i < wordList.size(); ++i) {
		qDebug() << wordList.at(i);
	}
	QString tmpLine;
	if (fileOut.open(QFile::WriteOnly))
	{
		QTextStream streamText(&fileOut);
		for (int i = 0; i < wordList.size(); ++i) {
			tmpLine = wordList.at(i);
			if (i < 7) {
				streamText<< tmpLine << "\r\n";
			}
			else {
				if (dataFrames.size() == i - 7) {
					fileOut.close();
					return;
				}
				tmpLine = tmpLine.left(tmpLine.size() - 5);
				for (int h = 0; h < 6; ++h){
					double tmpVal = dataFrames.at(i - 7).at(h);
					if (h == 5){
						tmpLine = tmpLine + QString::number(tmpVal) ;
					}
					else {
						tmpLine = tmpLine + QString::number(tmpVal) + ",";
					}
				}
				streamText<< tmpLine << "\r\n";
			}
		}
		fileOut.close();
	}
	
	//qDebug() << wordList << '\n';
}
void calcErrorRate(const QString& filePath, const QString& outputFilePath)
{
	QFile fileCsv(filePath);
	if (!fileCsv.open(QIODevice::ReadWrite)){
		qDebug() << fileCsv.errorString();
		return;
	}
	QStringList wordList;
	while (!fileCsv.atEnd())
	{
		QByteArray line = fileCsv.readLine();
		wordList.append(line.split('\r\n').first());
	}

	QFile fileOut(outputFilePath);
	QString tmpString;
	for (int i = 0; i < wordList.size(); ++i) {
		//qDebug() << wordList.at(i);
	}
	QString tmpLine;
	if (fileOut.open(QFile::WriteOnly))
	{
		QTextStream streamText(&fileOut);
		for (int i = 0; i < wordList.size(); ++i) {
			tmpLine = wordList.at(i);
			
			if (i < 7) {
				streamText << tmpLine << "\r\n";
			}
			else {
			
				tmpLine = tmpLine.left(tmpLine.size() - 5);
				QStringList tmpStringList = tmpLine.split(",");
				qDebug() <<"LIne  "<< tmpLine;
				std::cout << "SIZE " << tmpStringList.size() << std::endl;
				for (int h = 0; h < 6; ++h){

					double maxEl = std::max(tmpStringList.at(9 + h).toFloat(), tmpStringList.at(15 + h).toFloat());
					double errorRt = (tmpStringList.at(9 + h).toFloat() - tmpStringList.at(15 + h).toFloat()) / tmpStringList.at(9 + h).toFloat();
					//double errorRt = tmpStringList.at(12 + h).toFloat()*1000;
					if (errorRt < 0){
						 errorRt = -errorRt;
					}
			
					if (h == 0){
						tmpLine =    tmpLine + QString::number(errorRt) + ",";// tmpLine + QString::number(tmpVal);
					
					}
					else if (h != 5) {
					
						tmpLine = tmpLine  + /*QString::number(tmpVal)*/ QString::number(errorRt) + ",";
					}
					else if(h == 5) {
						tmpLine = tmpLine + /*QString::number(tmpVal)*/ QString::number(errorRt) ;
						
					}
			
				
				}
			
        		streamText << tmpLine<< "\r\n";

			}
		}
		fileOut.close();
	}


}

#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtCore/QDebug>




void parseCSVFile(const QString& filePath, const QString& outputFilePAth,const std::vector<std::vector<double>>& dataFrames);
void calcErrorRate(const QString& filePath, const QString& outputFilePath);
